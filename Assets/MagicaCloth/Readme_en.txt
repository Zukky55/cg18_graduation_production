﻿//------------------------------------------------------------------------------
// Magica Cloth
// Copyright (c) Magica Soft, 2020
// https://magicasoft.jp
//------------------------------------------------------------------------------

### About
Magica Cloth is a high-speed cloth simulation operated by Unity Job System + Burst compiler.


### Support Unity versions
Unity2019.2.0 or higher


### Feature

* Fast cloth simulation with Unity Job System + Burst compiler
* Works on all platforms except WebGL
* Implement BoneCloth driven by Bone (Transform) and MeshCloth driven by mesh
* MeshCloth can also work with skinning mesh
* Easy setup with an intuitive interface
* Time operation such as slow is possible
* With full source code


### Documentation
Since it is an online manual, please refer to the following URL for details.
https://magicasoft.jp/magica-cloth


### Release Notes

[v1.0.0]
* first release.




