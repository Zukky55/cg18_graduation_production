﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// メッシュデータ
    /// </summary>
    public class PhysicsManagerMeshData : PhysicsManagerAccess
    {
        /// <summary>
        /// 管理中のメッシュデフォーマー
        /// </summary>
        HashSet<BaseMeshDeformer> meshSet = new HashSet<BaseMeshDeformer>();

        //=========================================================================================
        /// <summary>
        /// 共通メッシュフラグ
        /// </summary>
        public const uint MeshFlag_Active = 0x00000001;
        public const uint Meshflag_CalcNormalTangent = 0x00000002;
        public const uint MeshFlag_Skinning = 0x00000004;

        //=========================================================================================
        /// <summary>
        /// 仮想メッシュ共有情報
        /// </summary>
        public struct SharedVirtualMeshInfo
        {
            public int uid;

            // 参照カウンタ
            public int useCount;

            public int sharedChildMeshStartIndex;
            public int sharedChildMeshCount;

            // 頂点情報格納先(sharedVirtualUvList/sharedVirtualVertexInfoList)
            public ChunkData vertexChunk;

            // 頂点スキニング情報格納先(sharedVirtualWeightList)
            public ChunkData weightChunk;

            // トライアングル情報格納先(sharedVirtualTriangleList)
            public ChunkData triangleChunk;

            // 頂点が所属するトライアングルリスト(sharedVirtualVertexToTriangleInfoList)
            public ChunkData vertexToTriangleChunk;
        }
        public FixedNativeList<SharedVirtualMeshInfo> sharedVirtualMeshInfoList;
        public Dictionary<int, int> sharedVirtualMeshIdToIndexDict = new Dictionary<int, int>(); // 登録管理

        /// <summary>
        /// 頂点ごとのUV
        /// </summary>
        public FixedChunkNativeArray<float2> sharedVirtualUvList;

        /// <summary>
        /// 頂点ごとのウエイト数とウエイト情報開始インデックス
        /// 上位4bit = ウエイト数
        /// 下位28bit = ウエイトリストの開始インデックス
        /// </summary>
        public FixedChunkNativeArray<uint> sharedVirtualVertexInfoList;

        /// <summary>
        /// ウエイト情報リスト
        /// </summary>
        public FixedChunkNativeArray<MeshData.VertexWeight> sharedVirtualWeightList;

        /// <summary>
        /// トライアングル構成リスト（トライアングルごとに３つの頂点インデックス）
        /// </summary>
        public FixedChunkNativeArray<int> sharedVirtualTriangleList;

        /// <summary>
        /// 頂点が接続するトライアングルインデックスリストへの情報
        /// 上位8bit = 接続トライアングル数
        /// 下位24bit = 接続トライアングルリスト(sharedVirtualVertexToTriangleIndexList)の開始インデックス
        /// </summary>
        public FixedChunkNativeArray<uint> sharedVirtualVertexToTriangleInfoList;

        /// <summary>
        /// 頂点が接続するトライアングルインデックスリスト
        /// </summary>
        public FixedChunkNativeArray<int> sharedVirtualVertexToTriangleIndexList;

        //=========================================================================================
        /// <summary>
        /// 仮想メッシュインスタンス情報
        /// </summary>
        public struct VirtualMeshInfo
        {
            public uint flag;
            public int sharedVirtualMeshIndex;
            public int meshUseCount; // 利用カウント
            public int vertexUseCount; // 利用頂点数

            // 頂点情報格納先(useVertexList/posList/normalList/tangentList)
            public ChunkData vertexChunk;

            // ボーン情報格納先(transformIndexList)
            public ChunkData boneChunk;

            // トライアングル情報格納先(triangleNormalList)
            public ChunkData triangleChunk;

            /// <summary>
            /// フラグ判定
            /// </summary>
            /// <param name="flag"></param>
            /// <returns></returns>
            public bool IsFlag(uint flag)
            {
                return (this.flag & flag) != 0;
            }

            /// <summary>
            /// フラグ設定
            /// </summary>
            /// <param name="flag"></param>
            /// <param name="sw"></param>
            public void SetFlag(uint flag, bool sw)
            {
                if (sw)
                    this.flag |= flag;
                else
                    this.flag &= ~flag;
            }

            public bool IsActive()
            {
                return IsFlag(MeshFlag_Active);
            }

            public bool IsUse()
            {
                return IsFlag(MeshFlag_Active) && meshUseCount > 0 && vertexUseCount > 0;
            }
        }
        public FixedNativeList<VirtualMeshInfo> virtualMeshInfoList;

        /// <summary>
        /// 仮想メッシュインスタンスが接続しているレンダーメッシュ情報
        /// ※これはインスタンスごとのデータ
        /// x = レンダーメッシュインスタンスインデックス
        /// y = レンダーメッシュの頂点スタートインデックス
        /// z = レンダー共有メッシュの頂点スタートインデックス
        /// </summary>
        //public ExNativeMultiHashMap<int, int3> virtualToRenderMeshIndexMap;

        /// <summary>
        /// 頂点情報
        /// 上位16bit = 頂点が参照する仮想メッシュインスタンスインデックス
        ///             ※実際のインデックス＋１が入るので注意！（０＝無効とする）
        /// 下位16bit = 頂点利用数（カウント１以上のものを計算する）
        /// </summary>
        public FixedChunkNativeArray<uint> virtualVertexInfoList;

        /// <summary>
        /// 頂点フラグ
        /// </summary>
        public FixedChunkNativeArray<byte> virtualVertexFlagList;

        /// <summary>
        /// 現在のワールド頂点姿勢リスト
        /// </summary>
        public FixedChunkNativeArray<float3> virtualPosList;
        public FixedChunkNativeArray<quaternion> virtualRotList;

        /// <summary>
        /// 現在のボーン姿勢情報
        /// </summary>
        public FixedChunkNativeArray<int> virtualTransformIndexList;

        /// <summary>
        /// 現在のトライアングル法線接線
        /// </summary>
        public FixedChunkNativeArray<float3> virtualTriangleNormalList;
        public FixedChunkNativeArray<float3> virtualTriangleTangentList;

        /// <summary>
        /// トライアングルが参照する仮想メッシュインスタンスインデックス
        /// ※実際のインデックス＋１が入るので注意！（０＝無効とする）
        /// </summary>
        public FixedChunkNativeArray<ushort> virtualTriangleMeshIndexList;

        //=========================================================================================
        /// <summary>
        /// 仮想メッシュの子メッシュ共有情報
        /// </summary>
        public struct SharedChildMeshInfo
        {
            public long cuid;

            public int sharedVirtualMeshIndex;
            public int virtualMeshIndex;
            public int meshUseCount; // 利用カウント

            // 頂点情報格納先(レンダーメッシュと１：１に対応)
            public ChunkData vertexChunk;

            public ChunkData weightChunk;
        }
        public FixedNativeList<SharedChildMeshInfo> sharedChildMeshInfoList;
        public Dictionary<long, int> sharedChildMeshIdToSharedVirtualMeshIndexDict = new Dictionary<long, int>(); // 登録管理用

        /// <summary>
        /// 頂点ごとのウエイト数とウエイト情報開始インデックス
        /// 上位4bit = ウエイト数
        /// 下位28bit = ウエイトリストの開始インデックス
        /// </summary>
        public FixedChunkNativeArray<uint> sharedChildVertexInfoList;

        /// <summary>
        /// ウエイト情報リスト
        /// </summary>
        public FixedChunkNativeArray<MeshData.VertexWeight> sharedChildWeightList;

        //=========================================================================================
        /// <summary>
        /// レンダーメッシュの共有情報
        /// </summary>
        public struct SharedRenderMeshInfo
        {
            public int uid;

            // 参照カウンタ
            public int useCount;

            public uint flag;

            // 頂点情報格納先(vertices/normals/tangents/)
            public ChunkData vertexChunk;

            // ボーンウエイト格納先
            public ChunkData bonePerVertexChunk;    // (sharedBonesPerVertexList/sharedBonesPerVertexStartList)
            public ChunkData boneWeightsChunk;      // (sharedBoneWeightsList)

            // 各成分のデータ有無
            public bool existNormals;
            public bool existTangents;
            public bool existWeights;

            // レンダラートランスフォームのボーンインデックス
            public int rendererBoneIndex;

            /// <summary>
            /// フラグ判定
            /// </summary>
            /// <param name="flag"></param>
            /// <returns></returns>
            public bool IsFlag(uint flag)
            {
                return (this.flag & flag) != 0;
            }

            /// <summary>
            /// フラグ設定
            /// </summary>
            /// <param name="flag"></param>
            /// <param name="sw"></param>
            public void SetFlag(uint flag, bool sw)
            {
                if (sw)
                    this.flag |= flag;
                else
                    this.flag &= ~flag;
            }

            public bool IsSkinning()
            {
                return IsFlag(MeshFlag_Skinning);
            }
        }
        public FixedNativeList<SharedRenderMeshInfo> sharedRenderMeshInfoList;
        public Dictionary<int, int> sharedRenderMeshIdToIndexDict = new Dictionary<int, int>(); // 登録管理

        public FixedChunkNativeArray<float3> sharedRenderVertices;

        // 法線／接線
        public FixedChunkNativeArray<float3> sharedRenderNormals;
        public FixedChunkNativeArray<float4> sharedRenderTangents; // ここはオリジナルなのでw成分あり

        // ボーンウエイト（存在しない場合があるので注意）
        public FixedChunkNativeArray<byte> sharedBonesPerVertexList;
        public FixedChunkNativeArray<int> sharedBonesPerVertexStartList;
        public FixedChunkNativeArray<BoneWeight1> sharedBoneWeightList;

        /// <summary>
        /// レンダーメッシュのメッシュ書き込みバッファ
        /// </summary>
        public class SharedRenderMeshBuffer
        {
            public Vector3[] vertices;
            public Vector3[] normals;
            public Vector4[] tangents;
        }
        public Dictionary<int, SharedRenderMeshBuffer> sharedRenderMeshIdToBufferDict = new Dictionary<int, SharedRenderMeshBuffer>();

        //=========================================================================================
        /// <summary>
        /// レンダーメッシュ頂点フラグ
        /// </summary>
        public const uint RenderVertexFlag_Use = 0x00010000; // 使用フラグ

        /// <summary>
        /// レンダーメッシュインスタンス情報
        /// </summary>
        public struct RenderMeshInfo
        {
            public uint flag;

            public int renderSharedMeshIndex;
            public int sharedRenderMeshVertexStartIndex;

            public int meshUseCount; // 利用カウント

            // 頂点情報格納先(posList/normalList/tangentList)
            public ChunkData vertexChunk;

            // ボーンウエイト格納先(boneWeights)
            public ChunkData boneWeightsChunk;

            // レンダーメッシュトランスフォーム
            public int transformIndex;

            /// <summary>
            /// フラグ判定
            /// </summary>
            /// <param name="flag"></param>
            /// <returns></returns>
            public bool IsFlag(uint flag)
            {
                return (this.flag & flag) != 0;
            }

            /// <summary>
            /// フラグ設定
            /// </summary>
            /// <param name="flag"></param>
            /// <param name="sw"></param>
            public void SetFlag(uint flag, bool sw)
            {
                if (sw)
                    this.flag |= flag;
                else
                    this.flag &= ~flag;
            }

            public bool IsActive()
            {
                return IsFlag(MeshFlag_Active);
            }

            public bool IsUse()
            {
                return IsFlag(MeshFlag_Active) && meshUseCount > 0;
            }
        }
        public FixedNativeList<RenderMeshInfo> renderMeshInfoList;

        /// <summary>
        /// レンダーメッシュインスタンスが接続している仮想メッシュ情報
        /// ※これはレンダーメッシュインスタンスごとのデータ
        /// x = 子共有メッシュの頂点スタートインデックス
        /// y = 子共有メッシュのウエイトスタートインデック
        /// z = 仮想メッシュの頂点スタートインデックス
        /// w = 仮想共有メッシュの頂点スタートインデックス
        /// </summary>
        public ExNativeMultiHashMap<int, int4> renderToChildMeshIndexMap;

        /// <summary>
        /// 頂点ごとのフラグ
        /// 上位16bit = フラグ(RenderFlag_Use～)
        /// 下位16bit = 頂点が参照するレンダーメッシュインスタンスインデックス
        ///             ※実際のインデックス＋１が入るので注意！（０＝無効とする）
        /// </summary>
        public FixedChunkNativeArray<uint> renderVertexFlagList;

        /// <summary>
        /// 現在の頂点姿勢リスト
        /// 内部でワールド座標系で計算された後ローカル座標系に書き戻される
        /// </summary>
        public FixedChunkNativeArray<float3> renderPosList;
        public FixedChunkNativeArray<float3> renderNormalList;
        public FixedChunkNativeArray<float4> renderTangentList; // ここはメッシュに合わせるのでw成分あり

        /// <summary>
        /// 現在のボーンウエイトリスト
        /// </summary>
        public FixedChunkNativeArray<BoneWeight1> renderBoneWeightList;

        //=========================================================================================
        /// <summary>
        /// 初期設定
        /// </summary>
        public override void Create()
        {
            // shared virtual mesh
            sharedVirtualMeshInfoList = new FixedNativeList<SharedVirtualMeshInfo>();
            //virtualToRenderMeshIndexMap = new ExNativeMultiHashMap<int, int3>();
            sharedVirtualVertexInfoList = new FixedChunkNativeArray<uint>();
            sharedVirtualWeightList = new FixedChunkNativeArray<MeshData.VertexWeight>();
            sharedVirtualUvList = new FixedChunkNativeArray<float2>();
            sharedVirtualTriangleList = new FixedChunkNativeArray<int>();
            sharedVirtualVertexToTriangleInfoList = new FixedChunkNativeArray<uint>();
            sharedVirtualVertexToTriangleIndexList = new FixedChunkNativeArray<int>();

            // virtual mesh
            virtualMeshInfoList = new FixedNativeList<VirtualMeshInfo>();
            virtualVertexInfoList = new FixedChunkNativeArray<uint>();
            virtualVertexFlagList = new FixedChunkNativeArray<byte>();
            virtualPosList = new FixedChunkNativeArray<float3>();
            virtualRotList = new FixedChunkNativeArray<quaternion>();
            virtualTransformIndexList = new FixedChunkNativeArray<int>();
            virtualTriangleNormalList = new FixedChunkNativeArray<float3>();
            virtualTriangleTangentList = new FixedChunkNativeArray<float3>();
            virtualTriangleMeshIndexList = new FixedChunkNativeArray<ushort>();

            // shared virtual child mesh
            sharedChildMeshInfoList = new FixedNativeList<SharedChildMeshInfo>();
            sharedChildVertexInfoList = new FixedChunkNativeArray<uint>();
            sharedChildWeightList = new FixedChunkNativeArray<MeshData.VertexWeight>();

            // shared render mesh
            sharedRenderMeshInfoList = new FixedNativeList<SharedRenderMeshInfo>();
            sharedRenderVertices = new FixedChunkNativeArray<float3>();
            sharedRenderNormals = new FixedChunkNativeArray<float3>();
            sharedRenderTangents = new FixedChunkNativeArray<float4>();
            sharedBonesPerVertexList = new FixedChunkNativeArray<byte>();
            sharedBonesPerVertexStartList = new FixedChunkNativeArray<int>();
            sharedBoneWeightList = new FixedChunkNativeArray<BoneWeight1>();

            // render mesh
            renderMeshInfoList = new FixedNativeList<RenderMeshInfo>();
            renderToChildMeshIndexMap = new ExNativeMultiHashMap<int, int4>();
            renderVertexFlagList = new FixedChunkNativeArray<uint>();
            renderPosList = new FixedChunkNativeArray<float3>();
            renderNormalList = new FixedChunkNativeArray<float3>();
            renderTangentList = new FixedChunkNativeArray<float4>();
            renderBoneWeightList = new FixedChunkNativeArray<BoneWeight1>();
        }

        /// <summary>
        /// 破棄
        /// </summary>
        public override void Dispose()
        {
            // shared virtual mesh
            sharedVirtualMeshInfoList.Dispose();
            //virtualToRenderMeshIndexMap.Dispose();
            sharedVirtualVertexInfoList.Dispose();
            sharedVirtualWeightList.Dispose();
            sharedVirtualUvList.Dispose();
            sharedVirtualTriangleList.Dispose();
            sharedVirtualVertexToTriangleInfoList.Dispose();
            sharedVirtualVertexToTriangleIndexList.Dispose();

            // virtual mesh
            virtualMeshInfoList.Dispose();
            virtualVertexInfoList.Dispose();
            virtualVertexFlagList.Dispose();
            virtualPosList.Dispose();
            virtualRotList.Dispose();
            virtualTransformIndexList.Dispose();
            virtualTriangleNormalList.Dispose();
            virtualTriangleTangentList.Dispose();
            virtualTriangleMeshIndexList.Dispose();

            // shared virtual child mesh
            sharedChildMeshInfoList.Dispose();
            sharedChildVertexInfoList.Dispose();
            sharedChildWeightList.Dispose();

            // shared render mesh
            sharedRenderMeshInfoList.Dispose();
            sharedRenderVertices.Dispose();
            sharedRenderNormals.Dispose();
            sharedRenderTangents.Dispose();
            sharedBonesPerVertexList.Dispose();
            sharedBonesPerVertexStartList.Dispose();
            sharedBoneWeightList.Dispose();

            // render mesh
            renderMeshInfoList.Dispose();
            renderToChildMeshIndexMap.Dispose();
            renderVertexFlagList.Dispose();
            renderPosList.Dispose();
            renderNormalList.Dispose();
            renderTangentList.Dispose();
            renderBoneWeightList.Dispose();
        }

        //=========================================================================================
        /// <summary>
        /// メッシュを追加
        /// </summary>
        /// <param name="bmesh"></param>
        public void AddMesh(BaseMeshDeformer bmesh)
        {
            meshSet.Add(bmesh);
        }

        /// <summary>
        /// メッシュを削除
        /// </summary>
        /// <param name="bmesh"></param>
        public void RemoveMesh(BaseMeshDeformer bmesh)
        {
            if (meshSet.Contains(bmesh))
                meshSet.Remove(bmesh);
        }

        public bool ContainsMesh(BaseMeshDeformer bmesh)
        {
            return meshSet.Contains(bmesh);
        }

        //=========================================================================================
        /// <summary>
        /// 仮想メッシュの登録
        /// ここでは最低限の情報とデータ領域のみ確保する
        /// </summary>
        /// <param name="id">共有情報ユニークID</param>
        /// <param name="vertexCount"></param>
        /// <param name="boneCount"></param>
        /// <param name="triangleCount"></param>
        /// <returns></returns>
        public int AddVirtualMesh(
            int uid,
            int vertexCount,
            int weightCount,
            int boneCount,
            int triangleCount,
            int vertexToTriangleIndexCount
            )
        {
            // 仮想メッシュ共有情報登録
            int sharedMeshIndex = -1;
            if (uid != 0)
            {
                if (sharedVirtualMeshIdToIndexDict.ContainsKey(uid))
                {
                    // 既存
                    sharedMeshIndex = sharedVirtualMeshIdToIndexDict[uid];
                    var sminfo = sharedVirtualMeshInfoList[sharedMeshIndex];
                    sminfo.useCount++; // 参照カウンタ+
                    sharedVirtualMeshInfoList[sharedMeshIndex] = sminfo;
                }
                else
                {
                    // 新規
                    var sminfo = new SharedVirtualMeshInfo();
                    sminfo.uid = uid;
                    sminfo.useCount = 1;

                    // vertices
                    var oc = sharedVirtualVertexInfoList.AddChunk(vertexCount);
                    sharedVirtualUvList.AddChunk(vertexCount);
                    sharedVirtualVertexToTriangleInfoList.AddChunk(vertexCount);
                    sminfo.vertexChunk = oc;

                    // weight
                    oc = sharedVirtualWeightList.AddChunk(weightCount);
                    sminfo.weightChunk = oc;

                    // triangles
                    if (triangleCount > 0)
                    {
                        oc = sharedVirtualTriangleList.AddChunk(triangleCount * 3);
                        sminfo.triangleChunk = oc;
                    }

                    // vertexToTriangleIndex
                    if (vertexToTriangleIndexCount > 0)
                    {
                        oc = sharedVirtualVertexToTriangleIndexList.AddChunk(vertexToTriangleIndexCount);
                        sminfo.vertexToTriangleChunk = oc;
                    }

                    sharedMeshIndex = sharedVirtualMeshInfoList.Add(sminfo);
                    sharedVirtualMeshIdToIndexDict.Add(uid, sharedMeshIndex);
                }
            }

            // 仮想メッシュインスタンス登録
            var minfo = new VirtualMeshInfo();
            minfo.SetFlag(MeshFlag_Active, true);
            minfo.sharedVirtualMeshIndex = sharedMeshIndex;

            var c = virtualVertexInfoList.AddChunk(vertexCount);
            virtualVertexFlagList.AddChunk(vertexCount);
            virtualPosList.AddChunk(vertexCount);
            virtualRotList.AddChunk(vertexCount);
            minfo.vertexChunk = c;

            Debug.Assert(boneCount > 0);
            c = virtualTransformIndexList.AddChunk(boneCount);
            minfo.boneChunk = c;

            if (triangleCount > 0)
            {
                c = virtualTriangleNormalList.AddChunk(triangleCount);
                virtualTriangleTangentList.AddChunk(triangleCount);
                virtualTriangleMeshIndexList.AddChunk(triangleCount);
                minfo.triangleChunk = c;
            }

            int index = virtualMeshInfoList.Add(minfo);

            // 頂点／トライアングルの参照仮想メッシュインスタンスIDを設定する
            // （＋１）したものなので注意！
            uint vpack = DataUtility.Pack16(index + 1, 0);
            virtualVertexInfoList.Fill(minfo.vertexChunk, vpack);
            if (triangleCount > 0)
                virtualTriangleMeshIndexList.Fill(minfo.triangleChunk, (ushort)(index + 1));

            return index;
        }

        /// <summary>
        /// 仮想共有メッシュが存在するか判定する
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public bool IsEmptySharedVirtualMesh(int uid)
        {
            return sharedVirtualMeshIdToIndexDict.ContainsKey(uid) == false;
        }

        /// <summary>
        /// 仮想メッシュ共有データを設定する
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <param name="sharedVertices"></param>
        /// <param name="sharedNormals"></param>
        /// <param name="sharedTangents"></param>
        /// <param name="sharedBoneWeights"></param>
        public void SetSharedVirtualMeshData(
            int virtualMeshIndex,
            uint[] sharedVertexInfoList,
            MeshData.VertexWeight[] sharedWeightList,
            Vector2[] sharedUv,
            int[] sharedTriangles,
            uint[] vertexToTriangleInfoList,
            int[] vertexToTriangleIndexList
            )
        {
            var minfo = virtualMeshInfoList[virtualMeshIndex];
            Debug.Assert(minfo.sharedVirtualMeshIndex >= 0);
            var smdata = sharedVirtualMeshInfoList[minfo.sharedVirtualMeshIndex];

            // 共有メッシュデータは新規のみコピーする
            if (smdata.useCount == 1)
            {
                sharedVirtualVertexInfoList.ToJobArray().CopyFromFast(smdata.vertexChunk.startIndex, sharedVertexInfoList);
                sharedVirtualWeightList.ToJobArray().CopyFromFast(smdata.weightChunk.startIndex, sharedWeightList);

                if (sharedUv != null && sharedUv.Length > 0)
                    sharedVirtualUvList.ToJobArray().CopyFromFast(smdata.vertexChunk.startIndex, sharedUv);

                if (vertexToTriangleInfoList != null && vertexToTriangleInfoList.Length > 0)
                    sharedVirtualVertexToTriangleInfoList.ToJobArray().CopyFromFast(smdata.vertexChunk.startIndex, vertexToTriangleInfoList);

                if (vertexToTriangleIndexList != null && vertexToTriangleIndexList.Length > 0)
                    sharedVirtualVertexToTriangleIndexList.ToJobArray().CopyFromFast(smdata.vertexToTriangleChunk.startIndex, vertexToTriangleIndexList);

                if (sharedTriangles != null && sharedTriangles.Length > 0)
                    sharedVirtualTriangleList.ToJobArray().CopyFromFast(smdata.triangleChunk.startIndex, sharedTriangles);
            }
        }

        /// <summary>
        /// 仮想メッシュの解除
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        public void RemoveVirtualMesh(int virtualMeshIndex)
        {
            if (virtualMeshIndex < 0)
                return;
            if (virtualMeshInfoList.Exists(virtualMeshIndex))
            {
                var minfo = virtualMeshInfoList[virtualMeshIndex];

                // 共有メッシュ解除
                int sharedMeshIndex = minfo.sharedVirtualMeshIndex;
                if (sharedMeshIndex >= 0)
                {
                    var sminfo = sharedVirtualMeshInfoList[sharedMeshIndex];
                    sminfo.useCount--; // 参照カウンタ
                    if (sminfo.useCount == 0)
                    {
                        // 削除
                        sharedVirtualVertexInfoList.RemoveChunk(sminfo.vertexChunk.chunkNo);
                        sharedVirtualWeightList.RemoveChunk(sminfo.weightChunk.chunkNo);
                        sharedVirtualUvList.RemoveChunk(sminfo.vertexChunk.chunkNo);
                        sharedVirtualVertexToTriangleInfoList.RemoveChunk(sminfo.vertexChunk.chunkNo);

                        if (sminfo.triangleChunk.dataLength > 0)
                        {
                            sharedVirtualTriangleList.RemoveChunk(sminfo.triangleChunk.chunkNo);
                        }
                        if (sminfo.vertexToTriangleChunk.dataLength > 0)
                        {
                            sharedVirtualVertexToTriangleIndexList.RemoveChunk(sminfo.vertexToTriangleChunk.chunkNo);
                        }
                        sharedVirtualMeshInfoList.Remove(sharedMeshIndex);
                        sharedVirtualMeshIdToIndexDict.Remove(sminfo.uid);
                    }
                    else
                    {
                        sharedVirtualMeshInfoList[sharedMeshIndex] = sminfo;
                    }
                }

                // インスタンスメッシュ解除
                virtualVertexInfoList.RemoveChunk(minfo.vertexChunk.chunkNo);
                virtualVertexFlagList.RemoveChunk(minfo.vertexChunk.chunkNo);
                virtualPosList.RemoveChunk(minfo.vertexChunk.chunkNo);
                virtualRotList.RemoveChunk(minfo.vertexChunk.chunkNo);

                virtualTransformIndexList.RemoveChunk(minfo.boneChunk.chunkNo);

                if (minfo.triangleChunk.dataLength > 0)
                {
                    virtualTriangleNormalList.RemoveChunk(minfo.triangleChunk.chunkNo);
                    virtualTriangleTangentList.RemoveChunk(minfo.triangleChunk.chunkNo);
                    virtualTriangleMeshIndexList.RemoveChunk(minfo.triangleChunk.chunkNo);
                }

                //Debug.Log("Remove Mesh Chunk:" + meshChunkIndex);
                virtualMeshInfoList.Remove(virtualMeshIndex);
            }
        }

        public bool ExistsVirtualMesh(int virtualMeshIndex)
        {
            return virtualMeshInfoList.Exists(virtualMeshIndex);
        }

        public VirtualMeshInfo GetVirtualMeshInfo(int virtualMeshIndex)
        {
            return virtualMeshInfoList[virtualMeshIndex];
        }

        /// <summary>
        /// 仮想メッシュが使用され利用頂点が１以上かか判定する
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <returns></returns>
        public bool IsUseVirtualMesh(int virtualMeshIndex)
        {
            return virtualMeshInfoList[virtualMeshIndex].IsUse();
        }

        /// <summary>
        /// 仮想メッシュが有効か判定する
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <returns></returns>
        public bool IsActiveVirtualMesh(int virtualMeshIndex)
        {
            return virtualMeshInfoList[virtualMeshIndex].IsActive();
        }

        /// <summary>
        /// 仮想メッシュの有効状態設定
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <param name="sw"></param>
        public void SetVirtualMeshActive(int virtualMeshIndex, bool sw)
        {
            if (virtualMeshInfoList.Exists(virtualMeshIndex))
            {
                var minfo = virtualMeshInfoList[virtualMeshIndex];
                minfo.SetFlag(MeshFlag_Active, sw);
                virtualMeshInfoList[virtualMeshIndex] = minfo;
            }
        }

        public void AddUseVirtualMesh(int virtualMeshIndex)
        {
            if (virtualMeshInfoList.Exists(virtualMeshIndex))
            {
                var minfo = virtualMeshInfoList[virtualMeshIndex];
                minfo.meshUseCount++;
                virtualMeshInfoList[virtualMeshIndex] = minfo;
            }
        }

        public void RemoveUseVirtualMesh(int virtualMeshIndex)
        {
            if (virtualMeshInfoList.Exists(virtualMeshIndex))
            {
                var minfo = virtualMeshInfoList[virtualMeshIndex];
                minfo.meshUseCount--;
                Debug.Assert(minfo.meshUseCount >= 0);
                virtualMeshInfoList[virtualMeshIndex] = minfo;
            }
        }

        /// <summary>
        /// 仮想メッシュ使用頂点開始
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <param name="vindex"></param>
        /// <returns>新規登録ならばtrueを返す</returns>
        public bool AddUseVirtualVertex(int virtualMeshIndex, int vindex)
        {
            if (virtualMeshInfoList.Exists(virtualMeshIndex))
            {
                //Debug.Log("Add:" + meshChunkIndex + "," + vindex);
                var minfo = virtualMeshInfoList[virtualMeshIndex];
                minfo.vertexUseCount++;
                virtualMeshInfoList[virtualMeshIndex] = minfo;

                int index = minfo.vertexChunk.startIndex + vindex;

                uint value = virtualVertexInfoList[index] + 1;
                virtualVertexInfoList[index] = value;
                bool change = (value == 1);
                return change;
            }
            else
                return false;
        }

        /// <summary>
        /// 仮想メッシュ使用頂点解除
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <param name="vindex"></param>
        /// <returns>登録解除ならtrueを返す</returns>
        public bool RemoveUseVirtualVertex(int virtualMeshIndex, int vindex)
        {
            if (virtualMeshInfoList.Exists(virtualMeshIndex))
            {
                //Debug.Log("Rem:" + meshChunkIndex + "," + vindex);
                var minfo = virtualMeshInfoList[virtualMeshIndex];
                minfo.vertexUseCount--;
                virtualMeshInfoList[virtualMeshIndex] = minfo;

                int index = minfo.vertexChunk.startIndex + vindex;
                uint value = virtualVertexInfoList[index] - 1;
                virtualVertexInfoList[index] = value;
                bool change = (value == 0);

                return change;
            }
            else
                return false;
        }

        /// <summary>
        /// 仮想メッシュ座標計算結果をワールド姿勢で取得する
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <param name="vertices"></param>
        /// <param name="normals"></param>
        /// <param name="tangents"></param>
        public void CopyToVirtualMeshWorldData(int virtualMeshIndex, Vector3[] vertices, Vector3[] normals, Vector3[] tangents)
        {
            var minfo = virtualMeshInfoList[virtualMeshIndex];
            int start = minfo.vertexChunk.startIndex;
            virtualPosList.ToJobArray().CopyToFast(start, vertices);
            var fw = new float3(0, 0, 1);
            var up = new float3(0, 1, 0);
            for (int i = 0; i < minfo.vertexChunk.dataLength; i++)
            {
                var rot = virtualRotList[start + i];
                normals[i] = math.mul(rot, fw);
                tangents[i] = math.mul(rot, up);
            }
        }

        public int SharedVirtualMeshCount
        {
            get
            {
                return sharedVirtualMeshInfoList.Count;
            }
        }

        public int VirtualMeshCount
        {
            get
            {
                return virtualMeshInfoList.Count;
            }
        }

        public int VirtualMeshVertexCount
        {
            get
            {
                int cnt = 0;
                for (int i = 0; i < virtualMeshInfoList.Length; i++)
                    cnt += virtualMeshInfoList[i].vertexChunk.dataLength;
                return cnt;
            }
        }

        public int VirtualMeshTriangleCount
        {
            get
            {
                int cnt = 0;
                for (int i = 0; i < virtualMeshInfoList.Length; i++)
                    cnt += virtualMeshInfoList[i].triangleChunk.dataLength;
                return cnt;
            }
        }

        public int VirtualMeshVertexUseCount
        {
            get
            {
                // 少々重いので注意
                int cnt = 0;
                for (int i = 0; i < virtualVertexInfoList.Length; i++)
                    if ((virtualVertexInfoList[i] & 0xffff) > 0)
                        cnt++;
                return cnt;
            }
        }

        public int VirtualMeshUseCount
        {
            get
            {
                int cnt = 0;
                for (int i = 0; i < virtualMeshInfoList.Length; i++)
                    cnt += virtualMeshInfoList[i].IsUse() ? 1 : 0;
                return cnt;
            }
        }

        //=========================================================================================
        /// <summary>
        /// 仮想メッシュの子メッシュの登録
        /// ここでは最低限の情報とデータ領域のみ確保する
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <param name="vertexCount"></param>
        /// <returns></returns>
        public int AddSharedChildMesh(
            long cuid,
            int virtualMeshIndex,
            int vertexCount,
            int weightCount
            )
        {
            var minfo = virtualMeshInfoList[virtualMeshIndex];
            int sharedMergeMeshIndex = minfo.sharedVirtualMeshIndex;

            // 仮想メッシュ共有情報登録
            int sharedChildMeshIndex = -1;
            if (sharedChildMeshIdToSharedVirtualMeshIndexDict.ContainsKey(cuid))
            {
                // 既存
                sharedChildMeshIndex = sharedChildMeshIdToSharedVirtualMeshIndexDict[cuid];
                var sc_minfo = sharedChildMeshInfoList[sharedChildMeshIndex];
                sc_minfo.meshUseCount++; // 参照カウンタ+
                sharedChildMeshInfoList[sharedChildMeshIndex] = sc_minfo;
            }
            else
            {
                // 新規
                var sc_minfo = new SharedChildMeshInfo();
                sc_minfo.cuid = cuid;
                sc_minfo.sharedVirtualMeshIndex = sharedMergeMeshIndex;
                sc_minfo.virtualMeshIndex = virtualMeshIndex;
                sc_minfo.meshUseCount = 1;

                // vertices/normals/triangles/bindpose
                var oc = sharedChildVertexInfoList.AddChunk(vertexCount);
                sc_minfo.vertexChunk = oc;

                // weight
                oc = sharedChildWeightList.AddChunk(weightCount);
                sc_minfo.weightChunk = oc;

                sharedChildMeshIndex = sharedChildMeshInfoList.Add(sc_minfo);

                sharedChildMeshIdToSharedVirtualMeshIndexDict.Add(cuid, sharedChildMeshIndex);
            }

            return sharedChildMeshIndex;
        }

        public bool IsEmptySharedChildMesh(long cuid)
        {
            return sharedChildMeshIdToSharedVirtualMeshIndexDict.ContainsKey(cuid) == false;
        }

        /// <summary>
        /// 仮想メッシュの子メッシュ共有データを設定する
        /// </summary>
        /// <param name="meshChunkIndex"></param>
        /// <param name="sharedVertices"></param>
        /// <param name="sharedNormals"></param>
        /// <param name="sharedTangents"></param>
        /// <param name="sharedBoneWeights"></param>
        public void SetSharedChildMeshData(
            int sharedMeshIndex,
            uint[] sharedVertexInfoList,
            MeshData.VertexWeight[] sharedVertexWeightList
            )
        {
            var smdata = sharedChildMeshInfoList[sharedMeshIndex];

            // 共有メッシュデータは新規のみコピーする
            if (smdata.meshUseCount == 1)
            {
                sharedChildVertexInfoList.ToJobArray().CopyFromFast(smdata.vertexChunk.startIndex, sharedVertexInfoList);
                sharedChildWeightList.ToJobArray().CopyFromFast(smdata.weightChunk.startIndex, sharedVertexWeightList);
            }
        }

        public void RemoveSharedChildMesh(int sharedChildMeshIndex)
        {
            // 共有メッシュ解除
            var sc_minfo = sharedChildMeshInfoList[sharedChildMeshIndex];
            sc_minfo.meshUseCount--; // 参照カウンタ
            if (sc_minfo.meshUseCount == 0)
            {
                // 削除
                sharedChildVertexInfoList.RemoveChunk(sc_minfo.vertexChunk.chunkNo);
                sharedChildWeightList.RemoveChunk(sc_minfo.weightChunk.chunkNo);

                sharedChildMeshInfoList.Remove(sharedChildMeshIndex);

                sharedChildMeshIdToSharedVirtualMeshIndexDict.Remove(sc_minfo.cuid);
            }
            else
            {
                sharedChildMeshInfoList[sharedChildMeshIndex] = sc_minfo;
            }
        }

        public int SharedRenderMeshCount
        {
            get
            {
                return sharedRenderMeshInfoList.Count;
            }
        }

        public int SharedChildMeshCount
        {
            get
            {
                return sharedChildMeshInfoList.Count;
            }
        }


        //=========================================================================================
        /// <summary>
        /// レンダーメッシュの登録
        /// ここでは最低限の情報とデータ領域のみ確保する
        /// </summary>
        /// <param name="vertexCount"></param>
        /// <returns></returns>
        public int AddRenderMesh(
            int uid,
            bool isSkinning,
            int vertexCount,
            //Transform rendererTransform,
            int rendererBoneIndex,
            int boneWeightCount
            )
        {
            // レンダーメッシュ共有情報登録
            int sharedMeshIndex = -1;
            if (uid != 0)
            {
                if (sharedRenderMeshIdToIndexDict.ContainsKey(uid))
                {
                    // 既存
                    sharedMeshIndex = sharedRenderMeshIdToIndexDict[uid];
                    var sminfo = sharedRenderMeshInfoList[sharedMeshIndex];
                    sminfo.useCount++; // 参照カウンタ+
                    sharedRenderMeshInfoList[sharedMeshIndex] = sminfo;
                }
                else
                {
                    // 新規
                    var sminfo = new SharedRenderMeshInfo();
                    sminfo.uid = uid;
                    sminfo.useCount = 1;
                    sminfo.rendererBoneIndex = rendererBoneIndex;
                    if (isSkinning)
                        sminfo.SetFlag(MeshFlag_Skinning, true);

                    // vertices/normals/triangles/bindpose
                    var oc = sharedRenderVertices.AddChunk(vertexCount);
                    sharedRenderNormals.AddChunk(vertexCount);
                    sharedRenderTangents.AddChunk(vertexCount);
                    sminfo.vertexChunk = oc;

                    // ボーンウエイト
                    if (isSkinning)
                    {
                        var bc = sharedBonesPerVertexList.AddChunk(vertexCount);
                        sharedBonesPerVertexStartList.AddChunk(vertexCount);
                        var wc = sharedBoneWeightList.AddChunk(boneWeightCount);
                        sminfo.bonePerVertexChunk = bc;
                        sminfo.boneWeightsChunk = wc;
                    }

                    sharedMeshIndex = sharedRenderMeshInfoList.Add(sminfo);
                    sharedRenderMeshIdToIndexDict.Add(uid, sharedMeshIndex);

                    // メッシュバッファを作成する
                    var smbuffer = new SharedRenderMeshBuffer();
                    smbuffer.vertices = new Vector3[vertexCount];
                    smbuffer.normals = new Vector3[vertexCount];
                    smbuffer.tangents = new Vector4[vertexCount];
                    sharedRenderMeshIdToBufferDict.Add(uid, smbuffer);
                }
            }

            // レンダーメッシュインスタンス登録
            var minfo = new RenderMeshInfo();
            minfo.SetFlag(MeshFlag_Active, true);
            minfo.renderSharedMeshIndex = sharedMeshIndex;
            minfo.sharedRenderMeshVertexStartIndex = sharedRenderMeshInfoList[sharedMeshIndex].vertexChunk.startIndex;

            var c = renderVertexFlagList.AddChunk(vertexCount);
            renderPosList.AddChunk(vertexCount);
            renderNormalList.AddChunk(vertexCount);
            renderTangentList.AddChunk(vertexCount);
            if (isSkinning)
            {
                minfo.boneWeightsChunk = renderBoneWeightList.AddChunk(boneWeightCount);
            }

            // レンダーメッシュトランスフォーム登録
            //minfo.transformIndex = Bone.AddBone(rendererTransform);

            minfo.vertexChunk = c;

            int index = renderMeshInfoList.Add(minfo);

            // 頂点の参照マージメッシュインスタンスIDを設定する
            // （＋１）したものなので注意！
            uint flag = (uint)index + 1;
            renderVertexFlagList.Fill(minfo.vertexChunk, flag);

            return index;
        }

        /// <summary>
        /// レンダーメッシュのレンダラートランスフォームを登録する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="rendererTransform"></param>
        public void AddRenderMeshBone(int renderMeshIndex, Transform rendererTransform)
        {
            var minfo = renderMeshInfoList[renderMeshIndex];

            // レンダーメッシュトランスフォーム登録
            minfo.transformIndex = Bone.AddBone(rendererTransform);

            renderMeshInfoList[renderMeshIndex] = minfo;
        }

        public bool IsEmptySharedRenderMesh(int uid)
        {
            return sharedRenderMeshIdToIndexDict.ContainsKey(uid) == false;
        }

        /// <summary>
        /// レンダーメッシュの共有データを設定する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="sharedVertices"></param>
        /// <param name="sharedNormals"></param>
        /// <param name="sharedTangents"></param>
        /// <param name="sharedBoneWeights"></param>
        public void SetRenderSharedMeshData(
            int renderMeshIndex,
            Vector3[] sharedVertices,
            Vector3[] sharedNormals,
            Vector4[] sharedTangents,
            NativeArray<byte> sharedBonesPerVertex,
            NativeArray<BoneWeight1> sharedBoneWeights
            )
        {
            var minfo = renderMeshInfoList[renderMeshIndex];
            Debug.Assert(minfo.renderSharedMeshIndex >= 0);
            var smdata = sharedRenderMeshInfoList[minfo.renderSharedMeshIndex];

            // 共有メッシュデータは新規のみコピーする
            if (smdata.useCount == 1)
            {
                sharedRenderVertices.ToJobArray().CopyFromFast(smdata.vertexChunk.startIndex, sharedVertices);

                // 法線は存在しない場合がある
                if (sharedNormals != null && sharedNormals.Length > 0)
                {
                    sharedRenderNormals.ToJobArray().CopyFromFast(smdata.vertexChunk.startIndex, sharedNormals);
                    smdata.existNormals = true;
                }

                // 接線は存在しない場合がある
                if (sharedTangents != null && sharedTangents.Length > 0)
                {
                    sharedRenderTangents.ToJobArray().CopyFromFast(smdata.vertexChunk.startIndex, sharedTangents);
                    smdata.existTangents = true;
                }

                // ボーンウエイトは存在しない場合がある
                if (sharedBonesPerVertex.Length > 0)
                {
                    int vcnt = sharedBonesPerVertex.Length;

                    // 各頂点のウエイトデータ開始インデックスを算出
                    int[] startIndexList = new int[vcnt];
                    int sindex = 0;
                    for (int i = 0; i < vcnt; i++)
                    {
                        startIndexList[i] = sindex;
                        sindex += sharedBonesPerVertex[i];
                    }

                    sharedBonesPerVertexList.ToJobArray().CopyFromFast(smdata.bonePerVertexChunk.startIndex, sharedBonesPerVertex.ToArray());
                    sharedBonesPerVertexStartList.ToJobArray().CopyFromFast(smdata.bonePerVertexChunk.startIndex, startIndexList);
                    sharedBoneWeightList.ToJobArray().CopyFromFast(smdata.boneWeightsChunk.startIndex, sharedBoneWeights.ToArray());
                }

                sharedRenderMeshInfoList[minfo.renderSharedMeshIndex] = smdata;
            }
        }

        /// <summary>
        /// レンダーメッシュの解除
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        public void RemoveRenderMesh(int renderMeshIndex)
        {
            if (renderMeshIndex < 0)
                return;
            if (renderMeshInfoList.Exists(renderMeshIndex))
            {
                var minfo = renderMeshInfoList[renderMeshIndex];

                // 共有メッシュ解除
                int sharedMeshIndex = minfo.renderSharedMeshIndex;
                if (sharedMeshIndex >= 0)
                {
                    var sminfo = sharedRenderMeshInfoList[sharedMeshIndex];
                    sminfo.useCount--; // 参照カウンタ
                    if (sminfo.useCount == 0)
                    {
                        // 削除
                        sharedRenderVertices.RemoveChunk(sminfo.vertexChunk.chunkNo);
                        sharedRenderNormals.RemoveChunk(sminfo.vertexChunk.chunkNo);
                        sharedRenderTangents.RemoveChunk(sminfo.vertexChunk.chunkNo);

                        if (sminfo.bonePerVertexChunk.dataLength > 0)
                        {
                            sharedBonesPerVertexList.RemoveChunk(sminfo.bonePerVertexChunk);
                            sharedBonesPerVertexStartList.RemoveChunk(sminfo.bonePerVertexChunk);
                        }
                        if (sminfo.boneWeightsChunk.dataLength > 0)
                        {
                            sharedBoneWeightList.RemoveChunk(sminfo.boneWeightsChunk);
                        }

                        sharedRenderMeshInfoList.Remove(sharedMeshIndex);
                        sharedRenderMeshIdToIndexDict.Remove(sminfo.uid);

                        // バッファ削除
                        sharedRenderMeshIdToBufferDict.Remove(sminfo.uid);
                    }
                    else
                    {
                        sharedRenderMeshInfoList[sharedMeshIndex] = sminfo;
                    }
                }

                // インスタンスメッシュ解除
                renderVertexFlagList.RemoveChunk(minfo.vertexChunk.chunkNo);
                renderPosList.RemoveChunk(minfo.vertexChunk.chunkNo);
                renderNormalList.RemoveChunk(minfo.vertexChunk.chunkNo);
                renderTangentList.RemoveChunk(minfo.vertexChunk.chunkNo);

                if (minfo.boneWeightsChunk.dataLength > 0)
                {
                    renderBoneWeightList.RemoveChunk(minfo.boneWeightsChunk);
                }

                // レンダーメッシュトランスフォーム解除
                //Bone.RemoveBone(minfo.transformIndex);

                // レンダーメッシュとマージメッシュのリンクをすべて削除
                renderToChildMeshIndexMap.Remove(renderMeshIndex);

                //Debug.Log("Remove Mesh Chunk:" + meshChunkIndex);
                renderMeshInfoList.Remove(renderMeshIndex);
            }
        }

        /// <summary>
        /// レンダーメッシュのレンダラートランスフォームを解除する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="rendererTransform"></param>
        public void RemoveRenderMeshBone(int renderMeshIndex)
        {
            var minfo = renderMeshInfoList[renderMeshIndex];

            // レンダーメッシュトランスフォーム解除
            Bone.RemoveBone(minfo.transformIndex);
            minfo.transformIndex = 0;

            renderMeshInfoList[renderMeshIndex] = minfo;
        }

        /// <summary>
        /// レンダーメッシュが使用され利用頂点が１以上かか判定する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <returns></returns>
        public bool IsUseRenderMesh(int renderMeshIndex)
        {
            return renderMeshInfoList[renderMeshIndex].IsUse();
        }

        /// <summary>
        /// レンダーメッシュが有効か判定する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <returns></returns>
        public bool IsActiveRenderMesh(int renderMeshIndex)
        {
            return renderMeshInfoList[renderMeshIndex].IsActive();
        }

        /// <summary>
        /// レンダーメッシュのフラグ設定
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="flag"></param>
        /// <param name="sw"></param>
        public void SetRenderMeshFlag(int renderMeshIndex, uint flag, bool sw)
        {
            if (renderMeshInfoList.Exists(renderMeshIndex))
            {
                var minfo = renderMeshInfoList[renderMeshIndex];
                minfo.SetFlag(flag, sw);
                renderMeshInfoList[renderMeshIndex] = minfo;
            }
        }

        public bool IsRenderMeshFlag(int renderMeshIndex, uint flag)
        {
            if (renderMeshInfoList.Exists(renderMeshIndex))
            {
                var minfo = renderMeshInfoList[renderMeshIndex];
                return minfo.IsFlag(flag);
            }
            return false;
        }

        /// <summary>
        /// レンダーメッシュの有効状態設定
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="sw"></param>
        public void SetRenderMeshActive(int renderMeshIndex, bool sw)
        {
            if (renderMeshInfoList.Exists(renderMeshIndex))
            {
                var minfo = renderMeshInfoList[renderMeshIndex];
                minfo.SetFlag(MeshFlag_Active, sw);
                renderMeshInfoList[renderMeshIndex] = minfo;
            }
        }

        public void AddUseRenderMesh(int renderMeshIndex)
        {
            if (renderMeshInfoList.Exists(renderMeshIndex))
            {
                var minfo = renderMeshInfoList[renderMeshIndex];
                minfo.meshUseCount++;
                renderMeshInfoList[renderMeshIndex] = minfo;
            }
        }

        public void RemoveUseRenderMesh(int renderMeshIndex)
        {
            if (renderMeshInfoList.Exists(renderMeshIndex))
            {
                var minfo = renderMeshInfoList[renderMeshIndex];
                minfo.meshUseCount--;
                Debug.Assert(minfo.meshUseCount >= 0);
                renderMeshInfoList[renderMeshIndex] = minfo;
            }
        }

        public void LinkRenderMesh(int renderMeshIndex, int childMeshVertexStart, int childMeshWeightStart, int virtualMeshVertexStart, int sharedVirtualMeshVertexStart)
        {
            // レンダーメッシュにマージ子メッシュを接続する
            renderToChildMeshIndexMap.Add(renderMeshIndex, new int4(childMeshVertexStart, childMeshWeightStart, virtualMeshVertexStart, sharedVirtualMeshVertexStart));
        }

        public void UnlinkRenderMesh(int renderMeshIndex, int childMeshVertexStart, int childMeshWeightStart, int virtualMeshVertexStart, int sharedVirtualMeshVertexStart)
        {
            // レンダーメッシュからマージ子メッシュの接続を解除する
            renderToChildMeshIndexMap.Remove(renderMeshIndex, new int4(childMeshVertexStart, childMeshWeightStart, virtualMeshVertexStart, sharedVirtualMeshVertexStart));
        }

        /// <summary>
        /// レンダーメッシュ座標計算結果をローカル姿勢で取得する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="vertices"></param>
        /// <param name="normals"></param>
        /// <param name="tangents"></param>
        public void CopyToRenderMeshLocalPositionData(int renderMeshIndex, Mesh mesh)
        {
            var minfo = renderMeshInfoList[renderMeshIndex];
            var sminfo = sharedRenderMeshInfoList[minfo.renderSharedMeshIndex];
            var buffer = sharedRenderMeshIdToBufferDict[sminfo.uid];
            renderPosList.ToJobArray().CopyToFast(minfo.vertexChunk.startIndex, buffer.vertices);
            mesh.vertices = buffer.vertices;
        }

        /// <summary>
        /// レンダーメッシュ座標計算結果をローカル姿勢で取得する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="vertices"></param>
        /// <param name="normals"></param>
        /// <param name="tangents"></param>
        public void CopyToRenderMeshLocalNormalTangentData(int renderMeshIndex, Mesh mesh)
        {
            var minfo = renderMeshInfoList[renderMeshIndex];
            var sminfo = sharedRenderMeshInfoList[minfo.renderSharedMeshIndex];
            var buffer = sharedRenderMeshIdToBufferDict[sminfo.uid];
            if (sminfo.existNormals)
            {
                renderNormalList.ToJobArray().CopyToFast(minfo.vertexChunk.startIndex, buffer.normals);
                mesh.normals = buffer.normals;
            }
            if (sminfo.existTangents)
            {
                renderTangentList.ToJobArray().CopyToFast(minfo.vertexChunk.startIndex, buffer.tangents);
                mesh.tangents = buffer.tangents;
            }
        }

        /// <summary>
        /// レンダーメッシュのボーンウエイト計算結果を取得する
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="vertices"></param>
        public void CopyToRenderMeshBoneWeightData(int renderMeshIndex, Mesh mesh, Mesh sharedMesh)
        {
            var minfo = renderMeshInfoList[renderMeshIndex];
            var sminfo = sharedRenderMeshInfoList[minfo.renderSharedMeshIndex];

            NativeArray<BoneWeight1> weights = new NativeArray<BoneWeight1>(minfo.boneWeightsChunk.dataLength, Allocator.Temp);
            renderBoneWeightList.ToJobArray().CopyToFast(minfo.boneWeightsChunk.startIndex, weights);
            mesh.SetBoneWeights(sharedMesh.GetBonesPerVertex(), weights);
        }

        /// <summary>
        /// レンダーメッシュ座標計算結果をワールド姿勢で取得する（エディタ用）
        /// </summary>
        /// <param name="renderMeshIndex"></param>
        /// <param name="vertices"></param>
        /// <param name="normals"></param>
        /// <param name="tangents"></param>
        public void CopyToRenderMeshWorldData(int renderMeshIndex, Transform target, Vector3[] vertices, Vector3[] normals, Vector3[] tangents)
        {
            var minfo = renderMeshInfoList[renderMeshIndex];

            // ローカル座標系データ取得
            renderPosList.ToJobArray().CopyToFast(minfo.vertexChunk.startIndex, vertices);
            renderNormalList.ToJobArray().CopyToFast(minfo.vertexChunk.startIndex, normals);
            Vector4[] tan4array = new Vector4[minfo.vertexChunk.dataLength];
            renderTangentList.ToJobArray().CopyToFast(minfo.vertexChunk.startIndex, tan4array);

            // ワールド座標変換
            for (int i = 0; i < minfo.vertexChunk.dataLength; i++)
            {
                vertices[i] = target.TransformPoint(vertices[i]);
                normals[i] = target.InverseTransformDirection(normals[i]);
                tangents[i] = target.InverseTransformDirection(tan4array[i]);
            }
        }

        public int RenderMeshCount
        {
            get
            {
                return renderMeshInfoList.Count;
            }
        }

        public int RenderMeshVertexCount
        {
            get
            {
                int cnt = 0;
                for (int i = 0; i < renderMeshInfoList.Length; i++)
                    cnt += renderMeshInfoList[i].vertexChunk.dataLength;
                return cnt;
            }
        }

        public int RenderMeshUseCount
        {
            get
            {
                int cnt = 0;
                for (int i = 0; i < renderMeshInfoList.Length; i++)
                    cnt += renderMeshInfoList[i].IsUse() ? 1 : 0;
                return cnt;
            }
        }

        public int RenderMeshVertexUseCount
        {
            get
            {
                // 重いので注意
                int cnt = 0;
                for (int i = 0; i < renderVertexFlagList.Length; i++)
                    if ((renderVertexFlagList[i] & RenderVertexFlag_Use) != 0)
                        cnt++;
                return cnt;
            }
        }

        //=========================================================================================
        /// <summary>
        /// 利用ボーンの登録
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        /// <param name="boneList"></param>
        public void SetVirtualMeshBone(int virtualMeshIndex, List<Transform> boneList)
        {
            if (virtualMeshInfoList.Exists(virtualMeshIndex))
            {
                var minfo = virtualMeshInfoList[virtualMeshIndex];

                for (int i = 0; i < boneList.Count; i++)
                {
                    virtualTransformIndexList[minfo.boneChunk.startIndex + i] = Bone.AddBone(boneList[i]);
                }
            }
        }

        //public void SetVirtualMeshBone(int virtualMeshIndex, Transform bone)
        //{
        //    if (virtualMeshInfoList.Exists(virtualMeshIndex))
        //    {
        //        var minfo = virtualMeshInfoList[virtualMeshIndex];

        //        virtualTransformIndexList[minfo.boneChunk.startIndex] = Bone.AddBone(bone);
        //    }
        //}

        /// <summary>
        /// 利用ボーンの解除
        /// </summary>
        /// <param name="virtualMeshIndex"></param>
        public void ResetVirtualMeshBone(int virtualMeshIndex)
        {
            if (virtualMeshIndex >= 0)
            {
                if (virtualMeshInfoList.Exists(virtualMeshIndex))
                {
                    var minfo = virtualMeshInfoList[virtualMeshIndex];

                    for (int i = 0; i < minfo.boneChunk.dataLength; i++)
                    {
                        int tindex = virtualTransformIndexList[minfo.boneChunk.startIndex + i];
                        Bone.RemoveBone(tindex);
                        virtualTransformIndexList[minfo.boneChunk.startIndex + i] = 0;
                    }
                }
            }
        }

        //=========================================================================================
        /// <summary>
        /// 物理計算完了後のメッシュ後処理
        /// </summary>
        public void FinishMesh()
        {
            foreach (var bmesh in meshSet)
            {
                if (bmesh != null)
                {
                    bmesh.Finish();
                }
            }
        }
    }
}
