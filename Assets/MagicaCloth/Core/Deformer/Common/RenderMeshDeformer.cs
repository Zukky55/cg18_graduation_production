﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// レンダラーメッシュデフォーマー
    /// </summary>
    [System.Serializable]
    public class RenderMeshDeformer : BaseMeshDeformer
    {
        /// <summary>
        /// データバージョン
        /// </summary>
        private const int DATA_VERSION = 2;

        /// <summary>
        /// 再計算モード
        /// </summary>
        public enum RecalculateMode
        {
            // なし
            None,

            // 再計算あり
            UpdatePerFrame,
        }

        // 法線/接線更新モード
        [SerializeField]
        private RecalculateMode normalAndTangentUpdateMode = RecalculateMode.UpdatePerFrame;

        [SerializeField]
        private Mesh sharedMesh = null;

        // ランタイムデータ //////////////////////////////////////////
        // 書き込み用
        MeshFilter meshFilter;
        SkinnedMeshRenderer skinMeshRenderer;
        Transform[] originalBones;
        Transform[] boneList;
        Mesh mesh;

        // メッシュ状態変更フラグ
        public bool IsChangePosition { get; set; }
        public bool IsChangeNormalTangent { get; set; }
        public bool IsChangeBoneWeights { get; set; }
        bool oldUse;

        //=========================================================================================
        /// <summary>
        /// データを識別するハッシュコードを作成して返す
        /// </summary>
        /// <returns></returns>
        public override int GetDataHash()
        {
            int hash = base.GetDataHash();
            hash += sharedMesh.GetDataHash();
            return hash;
        }

        //=========================================================================================
        public Mesh SharedMesh
        {
            get
            {
                return sharedMesh;
            }
        }

        //=========================================================================================
        public void OnValidate()
        {
            if (Application.isPlaying == false)
                return;

            if (status.IsActive)
            {
                // 法線／接線再計算モード設定
                SetRecalculateNormalAndTangentMode();
            }
        }

        /// <summary>
        /// 初期化
        /// </summary>
        protected override void OnInit()
        {
            base.OnInit();
            if (status.IsInitError)
                return;

            // レンダラーチェック
            if (TargetObject == null)
            {
                status.SetInitError();
                return;
            }
            var ren = TargetObject.GetComponent<Renderer>();
            if (ren == null)
            {
                status.SetInitError();
                return;
            }

            if (MeshData.VerifyData() == false)
            {
                status.SetInitError();
                return;
            }

            VertexCount = MeshData.VertexCount;
            TriangleCount = MeshData.TriangleCount;

            // クローンメッシュ作成
            // ここではメッシュは切り替えない
            mesh = null;
            if (ren is SkinnedMeshRenderer)
            {
                var sren = ren as SkinnedMeshRenderer;
                skinMeshRenderer = sren;

                // メッシュクローン
                mesh = GameObject.Instantiate(sharedMesh);
                mesh.MarkDynamic();
                originalBones = sren.bones;

                // クローンメッシュ初期化
                // srenのボーンリストはここで配列を作成し最後にレンダラーのトランスフォームを追加する
                var blist = new List<Transform>(originalBones);
                blist.Add(ren.transform); // レンダラートランスフォームを最後に追加
                boneList = blist.ToArray();

                var bindlist = new List<Matrix4x4>(sharedMesh.bindposes);
                bindlist.Add(Matrix4x4.identity); // レンダラーのバインドポーズを最後に追加
                mesh.bindposes = bindlist.ToArray();
            }
            else
            {
                // メッシュクローン
                mesh = GameObject.Instantiate(sharedMesh);
                mesh.MarkDynamic();

                meshFilter = TargetObject.GetComponent<MeshFilter>();
                Debug.Assert(meshFilter);
            }
            oldUse = false;

            // 共有メッシュのuid
            int uid = sharedMesh.GetInstanceID(); // 共有メッシュのIDを使う
            bool first = MagicaPhysicsManager.Instance.Mesh.IsEmptySharedRenderMesh(uid);

            // メッシュ登録
            MeshIndex = MagicaPhysicsManager.Instance.Mesh.AddRenderMesh(
                uid,
                MeshData.isSkinning,
                MeshData.VertexCount,
                //ren.transform,
                IsSkinning ? boneList.Length - 1 : 0, // レンダラーのボーンインデックス
                IsSkinning ? sharedMesh.GetAllBoneWeights().Length : 0
                );

            // レンダーメッシュの共有データを一次元配列にコピーする
            if (first)
            {
                MagicaPhysicsManager.Instance.Mesh.SetRenderSharedMeshData(
                    MeshIndex,
                    mesh.vertices,
                    mesh.normals,
                    mesh.tangents,
                    sharedMesh.GetBonesPerVertex(),
                    sharedMesh.GetAllBoneWeights()
                    );
            }

            // 法線／接線再計算モード設定
            SetRecalculateNormalAndTangentMode();
        }

        /// <summary>
        /// 実行状態に入った場合に呼ばれます
        /// </summary>
        protected override void OnActive()
        {
            base.OnActive();
            if (status.IsInitSuccess)
            {
                MagicaPhysicsManager.Instance.Mesh.SetRenderMeshActive(MeshIndex, true);

                // レンダラートランスフォーム登録
                MagicaPhysicsManager.Instance.Mesh.AddRenderMeshBone(MeshIndex, TargetObject.transform);
            }
        }

        /// <summary>
        /// 実行状態から抜けた場合に呼ばれます
        /// </summary>
        protected override void OnInactive()
        {
            base.OnInactive();
            if (status.IsInitSuccess)
            {
                if (MagicaPhysicsManager.IsInstance())
                {
                    // レンダラートランスフォーム解除
                    MagicaPhysicsManager.Instance.Mesh.RemoveRenderMeshBone(MeshIndex);

                    MagicaPhysicsManager.Instance.Mesh.SetRenderMeshActive(MeshIndex, false);
                }
            }
        }

        /// <summary>
        /// 破棄
        /// </summary>
        public override void Dispose()
        {
            if (MagicaPhysicsManager.IsInstance())
            {
                // メッシュ解除
                MagicaPhysicsManager.Instance.Mesh.RemoveRenderMesh(MeshIndex);
            }

            base.Dispose();
        }

        /// <summary>
        /// 法線／接線再計算モード設定
        /// </summary>
        void SetRecalculateNormalAndTangentMode()
        {
            // ジョブシステムを利用した法線／接線再計算設定
            MagicaPhysicsManager.Instance.Mesh.SetRenderMeshFlag(
                MeshIndex,
                PhysicsManagerMeshData.Meshflag_CalcNormalTangent,
                normalAndTangentUpdateMode == RecalculateMode.UpdatePerFrame
                );
        }

        //=========================================================================================
        public override bool IsMeshUse()
        {
            if (status.IsInitSuccess)
            {
                return MagicaPhysicsManager.Instance.Mesh.IsUseRenderMesh(MeshIndex);
            }

            return false;
        }

        public override bool IsActiveMesh()
        {
            if (status.IsInitSuccess)
            {
                return MagicaPhysicsManager.Instance.Mesh.IsActiveRenderMesh(MeshIndex);
            }

            return false;
        }

        public override void AddUseMesh(System.Object parent)
        {
            var virtualMeshDeformer = parent as VirtualMeshDeformer;
            Debug.Assert(virtualMeshDeformer != null);

            if (status.IsInitSuccess)
            {
                MagicaPhysicsManager.Instance.Mesh.AddUseRenderMesh(MeshIndex);
                IsChangePosition = true;
                IsChangeNormalTangent = true;
                IsChangeBoneWeights = true;

                // 親仮想メッシュと連動させる
                int virtualMeshIndex = virtualMeshDeformer.MeshIndex;
                var virtualMeshInfo = MagicaPhysicsManager.Instance.Mesh.virtualMeshInfoList[virtualMeshIndex];
                var sharedVirtualMeshInfo = MagicaPhysicsManager.Instance.Mesh.sharedVirtualMeshInfoList[virtualMeshInfo.sharedVirtualMeshIndex];
                int index = virtualMeshDeformer.GetRenderMeshDeformerIndex(this);
                long cuid = (long)sharedVirtualMeshInfo.uid << 16 + index;
                int sharedChildMeshIndex = MagicaPhysicsManager.Instance.Mesh.sharedChildMeshIdToSharedVirtualMeshIndexDict[cuid];
                var sharedChildMeshInfo = MagicaPhysicsManager.Instance.Mesh.sharedChildMeshInfoList[sharedChildMeshIndex];

                MagicaPhysicsManager.Instance.Mesh.LinkRenderMesh(
                    MeshIndex,
                    sharedChildMeshInfo.vertexChunk.startIndex,
                    sharedChildMeshInfo.weightChunk.startIndex,
                    virtualMeshInfo.vertexChunk.startIndex,
                    sharedVirtualMeshInfo.vertexChunk.startIndex
                    );

                // 利用頂点更新
                MagicaPhysicsManager.Instance.Compute.RenderMeshWorker.SetUpdateUseFlag();
            }
        }

        public override void RemoveUseMesh(System.Object parent)
        {
            //base.RemoveUseMesh();

            var virtualMeshDeformer = parent as VirtualMeshDeformer;
            Debug.Assert(virtualMeshDeformer != null);

            if (status.IsInitSuccess)
            {
                // 親仮想メッシュとの連動を解除する
                int virtualMeshIndex = virtualMeshDeformer.MeshIndex;
                var virtualMeshInfo = MagicaPhysicsManager.Instance.Mesh.virtualMeshInfoList[virtualMeshIndex];
                var sharedVirtualMeshInfo = MagicaPhysicsManager.Instance.Mesh.sharedVirtualMeshInfoList[virtualMeshInfo.sharedVirtualMeshIndex];
                int index = virtualMeshDeformer.GetRenderMeshDeformerIndex(this);
                long cuid = (long)sharedVirtualMeshInfo.uid << 16 + index;
                int sharedChildMeshIndex = MagicaPhysicsManager.Instance.Mesh.sharedChildMeshIdToSharedVirtualMeshIndexDict[cuid];
                var sharedChildMeshInfo = MagicaPhysicsManager.Instance.Mesh.sharedChildMeshInfoList[sharedChildMeshIndex];

                MagicaPhysicsManager.Instance.Mesh.UnlinkRenderMesh(
                    MeshIndex,
                    sharedChildMeshInfo.vertexChunk.startIndex,
                    sharedChildMeshInfo.weightChunk.startIndex,
                    virtualMeshInfo.vertexChunk.startIndex,
                    sharedVirtualMeshInfo.vertexChunk.startIndex
                    );


                MagicaPhysicsManager.Instance.Mesh.RemoveUseRenderMesh(MeshIndex);
                IsChangePosition = true;
                IsChangeNormalTangent = true;
                IsChangeBoneWeights = true;

                // 利用頂点更新
                MagicaPhysicsManager.Instance.Compute.RenderMeshWorker.SetUpdateUseFlag();
            }
        }

        //=========================================================================================
        /// <summary>
        /// メッシュ座標書き込み
        /// </summary>
        public override void Finish()
        {
            bool use = IsMeshUse();
#if true
            // メッシュ切替
            // 頂点変形が不要な場合は元の共有メッシュに戻す
            if (use != oldUse)
            {
                if (meshFilter)
                {
                    meshFilter.mesh = use ? mesh : sharedMesh;
                }
                else if (skinMeshRenderer)
                {
                    skinMeshRenderer.sharedMesh = use ? mesh : sharedMesh;
                    skinMeshRenderer.bones = use ? boneList : originalBones;
                }
                oldUse = use;

                if (use)
                {
                    IsChangePosition = true;
                    IsChangeNormalTangent = true;
                    IsChangeBoneWeights = true;
                }
            }

            if ((use || IsChangePosition || IsChangeNormalTangent) && mesh.isReadable)
            {
                // メッシュ書き戻し
                // meshバッファをmeshに設定する（重い！）
                // ★現状これ以外に方法がない！考えられる回避策は２つ：
                // ★（１）Unityの将来のバージョンでmeshのネイティブ配列がサポートされるのを待つ
                // ★（２）コンピュートバッファを使いシェーダーで頂点をマージする（かなり高速、しかしカスタムシェーダーが必須となり汎用性が無くなる）
                MagicaPhysicsManager.Instance.Mesh.CopyToRenderMeshLocalPositionData(MeshIndex, mesh);
                if (normalAndTangentUpdateMode == RecalculateMode.UpdatePerFrame)
                {
                    MagicaPhysicsManager.Instance.Mesh.CopyToRenderMeshLocalNormalTangentData(MeshIndex, mesh);
                }
                else if (IsChangeNormalTangent)
                {
                    // 元に戻す
                    mesh.normals = sharedMesh.normals;
                    mesh.tangents = sharedMesh.tangents;
                }
                IsChangePosition = false;
                IsChangeNormalTangent = false;
            }

            if (use && IsSkinning && IsChangeBoneWeights && mesh.isReadable)
            {
                // 頂点ウエイト変更
                MagicaPhysicsManager.Instance.Mesh.CopyToRenderMeshBoneWeightData(MeshIndex, mesh, sharedMesh);
                IsChangeBoneWeights = false;
            }
#endif
        }

        //=========================================================================================
        /// <summary>
        /// メッシュのワールド座標/法線/接線を返す（エディタ設定用）
        /// </summary>
        /// <param name="wposList"></param>
        /// <param name="wnorList"></param>
        /// <param name="wtanList"></param>
        /// <returns>頂点数</returns>
        public override int GetEditorPositionNormalTangent(
            out List<Vector3> wposList,
            out List<Vector3> wnorList,
            out List<Vector3> wtanList
            )
        {
            wposList = new List<Vector3>();
            wnorList = new List<Vector3>();
            wtanList = new List<Vector3>();

            if (Application.isPlaying)
            {
                if (IsMeshUse() == false || TargetObject == null)
                    return 0;

                Vector3[] posArray = new Vector3[VertexCount];
                Vector3[] norArray = new Vector3[VertexCount];
                Vector3[] tanArray = new Vector3[VertexCount];
                MagicaPhysicsManager.Instance.Mesh.CopyToRenderMeshWorldData(MeshIndex, TargetObject.transform, posArray, norArray, tanArray);

                wposList = new List<Vector3>(posArray);
                wnorList = new List<Vector3>(norArray);
                wtanList = new List<Vector3>(tanArray);

                return VertexCount;
            }
            else
            {
                if (TargetObject == null)
                {
                    return 0;
                }
                var ren = TargetObject.GetComponent<Renderer>();
                MeshUtility.CalcMeshWorldPositionNormalTangent(ren, sharedMesh, out wposList, out wnorList, out wtanList);

                return wposList.Count;
            }
        }

        /// <summary>
        /// メッシュのトライアングルリストを返す（エディタ設定用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorTriangleList()
        {
            if (sharedMesh)
            {
                return new List<int>(sharedMesh.triangles);
            }

            return null;
        }

        /// <summary>
        /// メッシュのラインリストを返す（エディタ用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorLineList()
        {
            // レンダーデフォーマーでは存在しない
            return null;
        }

        //=========================================================================================
        public override int GetVersion()
        {
            return DATA_VERSION;
        }

        /// <summary>
        /// 現在のデータが正常（実行できる状態）か返す
        /// </summary>
        /// <returns></returns>
        public override bool VerifyData()
        {
            if (base.VerifyData() == false)
                return false;

            if (sharedMesh == null)
                return false;

            return true;
        }

        /// <summary>
        /// データ情報
        /// </summary>
        /// <returns></returns>
        public override string GetInformation()
        {
            StaticStringBuilder.Clear();

            if (VerifyData())
            {
                // OK
                StaticStringBuilder.AppendLine("Skinning: ", MeshData.isSkinning);
                StaticStringBuilder.AppendLine("Vertex: ", MeshData.VertexCount);
                StaticStringBuilder.AppendLine("Triangle: ", MeshData.TriangleCount);
                StaticStringBuilder.Append("Bone: ", MeshData.BoneCount);
            }
            else
            {
                // エラー
                StaticStringBuilder.AppendLine("This mesh data is Invalid!");
                if (sharedMesh != null && sharedMesh.isReadable == false)
                {
                    StaticStringBuilder.AppendLine("Please turn On the [Read/Write Enabled] flag of the mesh importer.");
                }
                if (Application.isPlaying)
                {
                    StaticStringBuilder.Append("Execution stopped.");
                }
                else
                {
                    StaticStringBuilder.Append("Please create the mesh data.");
                }
            }

            return StaticStringBuilder.ToString();
        }
    }
}
