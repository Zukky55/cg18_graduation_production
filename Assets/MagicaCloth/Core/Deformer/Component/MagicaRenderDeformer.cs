﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// レンダーメッシュデフォーマーのコンポーネント
    /// </summary>
    [HelpURL("https://magicasoft.jp/magica-cloth-render-deformer/")]
    [AddComponentMenu("MagicaCloth/MagicaRenderDeformer")]
    public class MagicaRenderDeformer : CoreComponent
    {
        /// <summary>
        /// データバージョン
        /// </summary>
        private const int DATA_VERSION = 1;

        /// <summary>
        /// データリセットフラグ
        /// ※このフラグが立つとエディタ拡張側で自動的にデータが作成される
        /// </summary>
        [SerializeField]
        private bool dataReset;

        /// <summary>
        /// レンダーメッシュのデフォーマー
        /// </summary>
        [SerializeField]
        private RenderMeshDeformer deformer = new RenderMeshDeformer();

        [SerializeField]
        private int deformerHash;
        [SerializeField]
        private int deformerVersion;

        //=========================================================================================
        /// <summary>
        /// データを識別するハッシュコードを作成して返す
        /// </summary>
        /// <returns></returns>
        public override int GetDataHash()
        {
            int hash = 0;
            hash += deformer.GetDataHash();
            return hash;
        }

        //=========================================================================================
        public RenderMeshDeformer Deformer
        {
            get
            {
                return deformer;
            }
        }

        public bool DataReset
        {
            set
            {
                dataReset = value;
            }
            get
            {
                return dataReset;
            }
        }

        //=========================================================================================
        void Reset()
        {
            // 自動データ作成フラグを立てる
            DataReset = true;
        }

        void OnValidate()
        {
            deformer.OnValidate();
        }

        protected override void OnInit()
        {
            deformer.Init();
        }

        protected override void OnDispose()
        {
            deformer.Dispose();
        }

        protected override void OnUpdate()
        {
            deformer.Update();
        }

        protected override void OnActive()
        {
            deformer.OnEnable();
        }

        protected override void OnInactive()
        {
            deformer.OnDisable();
        }

        //=========================================================================================
        public override int GetVersion()
        {
            return DATA_VERSION;
        }

        /// <summary>
        /// データを検証して結果を格納する
        /// </summary>
        /// <returns></returns>
        public override void CreateVerifyData()
        {
            base.CreateVerifyData();
            deformerHash = deformer.SaveDataHash;
            deformerVersion = deformer.SaveDataVersion;
        }

        /// <summary>
        /// 現在のデータが正常（実行できる状態）か返す
        /// </summary>
        /// <returns></returns>
        public override bool VerifyData()
        {
            if (base.VerifyData() == false)
                return false;

            if (deformer == null)
                return false;

            if (deformer.VerifyData() == false)
                return false;

            if (deformerHash != deformer.SaveDataHash)
                return false;
            if (deformerVersion != deformer.SaveDataVersion)
                return false;

            return true;
        }

        public override string GetInformation()
        {
            if (deformer != null)
                return deformer.GetInformation();
            else
                return base.GetInformation();
        }

        //=========================================================================================
        /// <summary>
        /// メッシュのワールド座標/法線/接線を返す（エディタ用）
        /// </summary>
        /// <param name="wposList"></param>
        /// <param name="wnorList"></param>
        /// <param name="wtanList"></param>
        /// <returns>頂点数</returns>
        public override int GetEditorPositionNormalTangent(out List<Vector3> wposList, out List<Vector3> wnorList, out List<Vector3> wtanList)
        {
            return deformer.GetEditorPositionNormalTangent(out wposList, out wnorList, out wtanList);
        }

        /// <summary>
        /// メッシュのトライアングルリストを返す（エディタ用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorTriangleList()
        {
            return deformer.GetEditorTriangleList();
        }

        /// <summary>
        /// メッシュのラインリストを返す（エディタ用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorLineList()
        {
            return deformer.GetEditorLineList();
        }

        //=========================================================================================
        /// <summary>
        /// 頂点の使用状態をリストにして返す（エディタ用）
        /// 数値が１以上ならば使用中とみなす
        /// すべて使用状態ならばnullを返す
        /// </summary>
        /// <returns></returns>
        public override List<int> GetUseList()
        {
            return null;
        }

        //=========================================================================================
        /// <summary>
        /// 共有データオブジェクト収集
        /// </summary>
        /// <returns></returns>
        public override List<ShareDataObject> GetAllShareDataObject()
        {
            var slist = base.GetAllShareDataObject();
            slist.Add(deformer.MeshData);
            return slist;
        }
    }
}
