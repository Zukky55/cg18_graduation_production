﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// メッシュクロスのエディタ拡張
    /// </summary>
    [CustomEditor(typeof(MagicaMeshCloth))]
    public class MagicaMeshClothInspector : ClothEditor
    {
        protected override void OnEnable()
        {
            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;

            // データ状態
            EditorInspectorUtility.DispDataStatus(scr);

            serializedObject.Update();

            // データ検証
            if (EditorApplication.isPlaying == false)
                VerifyData();

            // モニターボタン
            EditorInspectorUtility.MonitorButtonInspector();

            // メイン
            MainInspector();

            // コライダー
            ColliderInspector();

            // パラメータ
            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorPresetUtility.DrawPresetButton(scr, scr.Params);
            {
                var cparam = serializedObject.FindProperty("clothParams");
                if (EditorInspectorUtility.RadiusInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.Radius);
                if (EditorInspectorUtility.MassInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.Mass);
                if (EditorInspectorUtility.GravityInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.Gravity);
                if (EditorInspectorUtility.DragInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.Drag);
                if (EditorInspectorUtility.MaxVelocityInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.MaxVelocity);
                if (EditorInspectorUtility.WorldInfluenceInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.WorldInfluence);
                if (EditorInspectorUtility.ClampDistanceInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.ClampDistance);
                if (EditorInspectorUtility.ClampPositionInspector(cparam, false))
                    scr.Params.SetChangeParam(ClothParams.ParamType.ClampPosition);
                if (EditorInspectorUtility.ClampRotationInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.ClampRotation);
                if (EditorInspectorUtility.RestoreDistanceInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.RestoreDistance);
                if (EditorInspectorUtility.RestoreRotationInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.RestoreRotation);
                if (EditorInspectorUtility.TriangleBendInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.TriangleBend);
                //if (EditorInspectorUtility.VolumeInspector(cparam))
                //    scr.Params.SetChangeParam(ClothParams.ParamType.Volume);
                if (EditorInspectorUtility.CollisionInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.ColliderCollision);
                if (EditorInspectorUtility.AdjustLineInspector(cparam))
                    scr.Params.SetChangeParam(ClothParams.ParamType.AdjustLine);
            }
            serializedObject.ApplyModifiedProperties();

            // データ作成
            if (EditorApplication.isPlaying == false)
            {
                EditorGUI.BeginDisabledGroup(CheckCreate() == false);

                EditorGUILayout.Space();
                EditorGUILayout.Space();

                GUI.backgroundColor = Color.red;
                if (GUILayout.Button("Crate"))
                {
                    Undo.RecordObject(scr, "CreateMeshCloth");
                    CreateData();
                }
                GUI.backgroundColor = Color.white;

                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.Space();
        }

        //=========================================================================================
        /// <summary>
        /// 作成を実行できるか判定する
        /// </summary>
        /// <returns></returns>
        protected override bool CheckCreate()
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;

            if (PointSelector.EditEnable)
                return false;

            if (scr.Deformer == null)
                return false;

            if (scr.Deformer.VerifyData() == false)
                return false;

            if (scr.IsValidPointSelect() == false)
                return false;

            return true;
        }

        /// <summary>
        /// データ検証
        /// </summary>
        private void VerifyData()
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;
            if (scr.VerifyData() == false)
            {
                // 検証エラー
                serializedObject.ApplyModifiedProperties();
            }
        }

        //=========================================================================================
        void MainInspector()
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;

            EditorGUILayout.LabelField("Main Setup", EditorStyles.boldLabel);

            // 仮想メッシュ
            EditorGUILayout.PropertyField(serializedObject.FindProperty("virtualDeformer"));

            EditorGUILayout.Space();

            // ポイント選択
            if (scr.Deformer != null)
            {
                EditorGUI.BeginDisabledGroup(!scr.Deformer.VerifyData());
                DrawInspectorGUI(scr.Deformer);
                EditorGUI.EndDisabledGroup();
            }
            EditorGUILayout.Space();
        }

        void ColliderInspector()
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;

            EditorGUILayout.Space();
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Collider List", EditorStyles.boldLabel);
            EditorInspectorUtility.DrawObjectList<ColliderComponent>(
                serializedObject.FindProperty("teamData.colliderList"),
                scr.gameObject,
                true, true
                );
        }

        //=============================================================================================
        /// <summary>
        /// 選択データの初期化
        /// 配列はすでに頂点数分が確保されゼロクリアされています。
        /// </summary>
        /// <param name="selectorData"></param>
        protected override void OnResetSelector(List<int> selectorData)
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;

            // すでに選択クラスがある場合は内容をコピーする
            if (scr.ClothSelection != null)
            {
                var sel = scr.ClothSelection.GetSelectionData(scr.Deformer.MeshData);
                for (int i = 0; i < selectorData.Count; i++)
                    selectorData[i] = sel[i];
            }
        }

        /// <summary>
        /// 選択データの決定
        /// </summary>
        /// <param name="selectorData"></param>
        protected override void OnFinishSelector(List<int> selectorData)
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;

            // 必ず新規データを作成する（ヒエラルキーでのコピー対策）
            var sel = CreateSelection(scr, "clothSelection");

            // 選択データコピー
            sel.SetSelectionData(scr.Deformer.MeshData, selectorData);

            // 現在のデータと比較し差異がない場合は抜ける
            if (scr.ClothSelection != null && scr.ClothSelection.Compare(sel))
                return;

            serializedObject.Update();
            if (scr.ClothSelection != null)
                Undo.RecordObject(scr.ClothSelection, "Set Selector");

            // 保存
            ApplySelection(scr, "clothSelection", sel);

            EditorUtility.SetDirty(scr.ClothSelection);
            serializedObject.ApplyModifiedProperties();
        }

        //=========================================================================================
        /// <summary>
        /// データ作成
        /// </summary>
        void CreateData()
        {
            MagicaMeshCloth scr = target as MagicaMeshCloth;

            Debug.Log("Started creating. [" + scr.name + "]");

            // チームハッシュを設定
            scr.TeamData.ValidateColliderList();

            // クロスデータ共有データ作成
            string dataname = "MeshClothData_" + scr.name;
            var cloth = ShareDataObject.CreateShareData<ClothData>(dataname);

            // クロスデータ用にセレクションデータを拡張する
            // （１）無効頂点の隣接が移動／固定頂点なら拡張に変更する
            // （２）移動／固定頂点に影響する子頂点に接続する無効頂点は拡張に変更する
            var selection = scr.Deformer.MeshData.ExtendSelection(scr.ClothSelection.GetSelectionData(scr.Deformer.MeshData), true, true);

            // クロスデータ作成
            cloth.CreateData(
                scr.Params,
                scr.TeamData,
                scr.Deformer.MeshData,
                scr.Deformer,
                selection
                );

            // クロスデータを設定
            var cdata = serializedObject.FindProperty("clothData");
            cdata.objectReferenceValue = cloth;
            serializedObject.ApplyModifiedProperties();

            // 検証
            scr.CreateVerifyData();
            serializedObject.ApplyModifiedProperties();

            EditorUtility.SetDirty(cloth);

            if (scr.VerifyData())
                Debug.Log("Creation completed. [" + scr.name + "]");
            else
                Debug.LogError("Creation failed.");
        }
    }
}