﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// メッシュクロス
    /// </summary>
    [HelpURL("https://magicasoft.jp/magica-cloth-mesh-cloth/")]
    [AddComponentMenu("MagicaCloth/MagicaMeshCloth", 100)]
    public class MagicaMeshCloth : BaseCloth
    {
        /// <summary>
        /// データバージョン
        /// </summary>
        private const int DATA_VERSION = 1;

        /// <summary>
        /// 仮想メッシュデフォーマー
        /// </summary>
        [SerializeField]
        private MagicaVirtualDeformer virtualDeformer = null;

        [SerializeField]
        private int virtualDeformerHash;
        [SerializeField]
        private int virtualDeformerVersion;

        //=========================================================================================
        /// <summary>
        /// データハッシュを求める
        /// </summary>
        /// <returns></returns>
        public override int GetDataHash()
        {
            int hash = base.GetDataHash();
            hash += virtualDeformer.GetDataHash();
            return hash;
        }

        //=========================================================================================
        public VirtualMeshDeformer Deformer
        {
            get
            {
                if (virtualDeformer != null)
                    return virtualDeformer.Deformer;
                return null;
            }
        }

        //=========================================================================================
        protected override void Reset()
        {
            base.Reset();
            ResetParams();
        }

        protected override void OnValidate()
        {
            base.OnValidate();
        }

        protected override void OnInit()
        {
            base.OnInit();
        }

        protected override void OnActive()
        {
            base.OnActive();
        }

        protected override void OnInactive()
        {
            base.OnInactive();
        }

        protected override void OnDispose()
        {
            base.OnDispose();
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
        }

        //=========================================================================================
        /// <summary>
        /// 頂点ごとのパーティクルフラグ設定（不要な場合は０）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override uint UserFlag(int index)
        {
            // メッシュクロスでは不要
            return 0;
        }

        /// <summary>
        /// 頂点ごとの連動トランスフォーム設定（不要な場合はnull）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override Transform UserTransform(int index)
        {
            // メッシュクロスでは不要
            return null;
        }

        /// <summary>
        /// 頂点ごとの連動トランスフォームのLocalPositionを返す（不要な場合は0）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override float3 UserTransformLocalPosition(int vindex)
        {
            // メッシュクロスでは不要
            return 0;
        }

        /// <summary>
        /// 頂点ごとの連動トランスフォームのLocalRotationを返す（不要な場合はquaternion.identity)
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override quaternion UserTransformLocalRotation(int vindex)
        {
            // メッシュクロスでは不要
            return quaternion.identity;
        }

        /// <summary>
        /// デフォーマーの数を返す
        /// </summary>
        /// <returns></returns>
        public override int GetDeformerCount()
        {
            return 1;
        }

        /// <summary>
        /// デフォーマーを返す
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override BaseMeshDeformer GetDeformer(int index)
        {
            return Deformer;
        }

        /// <summary>
        /// クロス初期化時に必要なMeshDataを返す（不要ならnull）
        /// </summary>
        /// <returns></returns>
        protected override MeshData GetMeshData()
        {
            return Deformer.MeshData;
        }

        /// <summary>
        /// クロス初期化の主にワーカーへの登録
        /// </summary>
        protected override void WorkerInit()
        {
            // デフォーマー頂点とパーティクルの連動登録
            var meshParticleWorker = MagicaPhysicsManager.Instance.Compute.MeshParticleWorker;
            var minfo = MagicaPhysicsManager.Instance.Mesh.GetVirtualMeshInfo(Deformer.MeshIndex);
            for (int i = 0; i < ClothData.VertexUseCount; i++)
            {
                int pindex = particleChunk.startIndex + i;
                int vindex = minfo.vertexChunk.startIndex + clothData.useVertexList[i];

                if (pindex >= 0)
                    meshParticleWorker.Add(TeamId, vindex, pindex);
            }
        }

        /// <summary>
        /// デフォーマーごとの使用頂点設定
        /// 使用頂点に対して AddUseVertex() / RemoveUseVertex() を実行する
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="deformer"></param>
        /// <param name="deformerIndex"></param>
        protected override void SetDeformerUseVertex(bool sw, BaseMeshDeformer deformer, int deformerIndex)
        {
            for (int i = 0; i < ClothData.VertexUseCount; i++)
            {
                // 未使用頂点は除く
                if (ClothData.IsInvalidVertex(i))
                    continue;

                int vindex = clothData.useVertexList[i];

                if (sw)
                    deformer.AddUseVertex(vindex);
                else
                    deformer.RemoveUseVertex(vindex);
            }
        }

        //=========================================================================================
        public override int GetVersion()
        {
            return DATA_VERSION;
        }

        /// <summary>
        /// データを検証して結果を格納する
        /// </summary>
        /// <returns></returns>
        public override void CreateVerifyData()
        {
            base.CreateVerifyData();
            virtualDeformerHash = virtualDeformer.SaveDataHash;
            virtualDeformerVersion = virtualDeformer.SaveDataVersion;
        }

        /// <summary>
        /// 現在のデータが正常（実行できる状態）か返す
        /// </summary>
        /// <returns></returns>
        public override bool VerifyData()
        {
            if (base.VerifyData() == false)
                return false;

            if (virtualDeformer == null)
                return false;
            if (virtualDeformer.VerifyData() == false)
                return false;
            if (virtualDeformerHash != virtualDeformer.SaveDataHash)
                return false;
            if (virtualDeformerVersion != virtualDeformer.SaveDataVersion)
                return false;

            return true;
        }

        /// <summary>
        /// データ検証の結果テキストを取得する
        /// </summary>
        /// <returns></returns>
        public override string GetInformation()
        {
            // 仮想デフォーマー情報
            StaticStringBuilder.Clear();

            if (VerifyData())
            {
                // OK
                StaticStringBuilder.AppendLine("Vertex: ", clothData.VertexUseCount);
                StaticStringBuilder.AppendLine("Clamp Distance: ", clothData.ClampDistanceConstraintCount);
                StaticStringBuilder.AppendLine("Clamp Position: ", clothParams.UseClampPositionLength ? clothData.VertexUseCount : 0);
                StaticStringBuilder.AppendLine("Clamp Rotation: ", clothData.ClampRotationConstraintRootCount, " - ", clothData.ClampRotationConstraintDataCount);
                StaticStringBuilder.AppendLine("Struct Distance: ", clothData.StructDistanceConstraintCount);
                StaticStringBuilder.AppendLine("Bend Distance: ", clothData.BendDistanceConstraintCount);
                StaticStringBuilder.AppendLine("Near Distance: ", clothData.NearDistanceConstraintCount);
                StaticStringBuilder.AppendLine("Restore Rotation: ", clothData.RestoreRotationConstraintCount);
                StaticStringBuilder.AppendLine("Triangle Bend: ", clothData.TriangleBendConstraintCount);
                StaticStringBuilder.AppendLine("Collider: ", teamData.ColliderCount);
                StaticStringBuilder.Append("Line Rotation: ", clothData.LineRotationWorkerCount);
            }
            else
            {
                // エラー
                StaticStringBuilder.AppendLine("This mesh cloth is in a state error!");
                if (Application.isPlaying)
                {
                    StaticStringBuilder.Append("Execution stopped.");
                }
                else
                {
                    StaticStringBuilder.Append("Please recreate the cloth data.");
                }
            }

            return StaticStringBuilder.ToString();
        }

        public bool IsValidPointSelect()
        {
            if (clothSelection == null)
                return false;

            if (Deformer.MeshData.ChildCount != clothSelection.DeformerCount)
                return false;

            return true;
        }

        //=========================================================================================
        /// <summary>
        /// メッシュのワールド座標/法線/接線を返す（エディタ用）
        /// </summary>
        /// <param name="wposList"></param>
        /// <param name="wnorList"></param>
        /// <param name="wtanList"></param>
        /// <returns>頂点数</returns>
        public override int GetEditorPositionNormalTangent(out List<Vector3> wposList, out List<Vector3> wnorList, out List<Vector3> wtanList)
        {
            return Deformer.GetEditorPositionNormalTangent(out wposList, out wnorList, out wtanList);
        }

        /// <summary>
        /// メッシュのトライアングルリストを返す（エディタ用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorTriangleList()
        {
            return Deformer.GetEditorTriangleList();
        }

        /// <summary>
        /// メッシュのラインリストを返す（エディタ用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorLineList()
        {
            return Deformer.GetEditorLineList();
        }

        //=========================================================================================
        /// <summary>
        /// 頂点の選択状態をリストにして返す（エディタ用）
        /// 選択状態は ClothSelection.Invalid / ClothSelection.Fixed / ClothSelection.Move
        /// すべてがInvalidならばnullを返す
        /// </summary>
        /// <returns></returns>
        public override List<int> GetSelectionList()
        {
            if (clothSelection != null && virtualDeformer != null && Deformer.MeshData != null)
                return clothSelection.GetSelectionData(Deformer.MeshData);
            else
                return null;
        }

        /// <summary>
        /// 頂点の使用状態をリストにして返す（エディタ用）
        /// 数値が１以上ならば使用中とみなす
        /// すべて使用状態ならばnullを返す
        /// </summary>
        /// <returns></returns>
        public override List<int> GetUseList()
        {
            if (Application.isPlaying && virtualDeformer != null)
            {
                if (Deformer != null)
                {
                    var minfo = MagicaPhysicsManager.Instance.Mesh.GetVirtualMeshInfo(Deformer.MeshIndex);
                    var infoList = MagicaPhysicsManager.Instance.Mesh.virtualVertexInfoList;

                    var useList = new List<int>();
                    for (int i = 0; i < minfo.vertexChunk.dataLength; i++)
                    {
                        uint data = infoList[minfo.vertexChunk.startIndex + i];
                        useList.Add((int)(data & 0xffff));
                    }
                    return useList;
                }
            }

            return null;
        }

        //=========================================================================================
        /// <summary>
        /// 共有データオブジェクト収集
        /// </summary>
        /// <returns></returns>
        public override List<ShareDataObject> GetAllShareDataObject()
        {
            var sdata = base.GetAllShareDataObject();
            sdata.Add(Deformer.MeshData);
            return sdata;
        }

        //=========================================================================================
        /// <summary>
        /// パラメータ初期化
        /// </summary>
        void ResetParams()
        {
            clothParams.SetRadius(0.02f, 0.02f);
            clothParams.SetMass(10.0f, 1.0f, true, -0.5f, true);
            clothParams.SetGravity(true, -9.8f, -9.8f);
            clothParams.SetDrag(true, 0.01f, 0.01f);
            clothParams.SetMaxVelocity(true, 3.0f, 3.0f);
            clothParams.SetWorldInfluence(0.5f, 0.5f);
            clothParams.SetTeleport(true);
            clothParams.SetClampDistanceRatio(true, 0.5f, 1.2f, 0.5f);
            clothParams.SetClampPositionLength(false, 0.0f, 0.4f, 0.5f);
            clothParams.SetClampRotationAngle(false, 30.0f, 30.0f, 0.3f);
            clothParams.SetRestoreDistance(1.0f);
            clothParams.SetRestoreRotation(false, 0.01f, 0.0f, 0.5f);
            clothParams.SetSpring(false);
            clothParams.SetAdjustRotation(false);
            clothParams.SetTriangleBend(true, 0.7f, 0.7f);
            clothParams.SetVolume(false);
            clothParams.SetCollision(false, 0.2f);
        }
    }
}
