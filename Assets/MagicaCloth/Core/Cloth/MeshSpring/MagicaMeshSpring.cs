﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// メッシュスプリング
    /// </summary>
    [HelpURL("https://magicasoft.jp/magica-cloth-mesh-spring/")]
    [AddComponentMenu("MagicaCloth/MagicaMeshSpring", 100)]
    public class MagicaMeshSpring : BaseCloth
    {
        /// <summary>
        /// データバージョン
        /// </summary>
        private const int DATA_VERSION = 1;

        // 対象仮想メッシュデフォーマー
        [SerializeField]
        private MagicaVirtualDeformer virtualDeformer = null;

        [SerializeField]
        private int virtualDeformerHash;
        [SerializeField]
        private int virtualDeformerVersion;

        // センタートランスフォーム
        [SerializeField]
        private Transform centerTransform;

        public enum Axis
        {
            X,
            Y,
            Z,
            InverseX,
            InverseY,
            InverseZ,
        }
        [SerializeField]
        private Axis directionAxis;

        [SerializeField]
        private SpringData springData = null;

        [SerializeField]
        private int springDataHash;
        [SerializeField]
        private int springDataVersion;

        //=========================================================================================
        public override int GetDataHash()
        {
            int hash = base.GetDataHash();
            hash += virtualDeformer.GetDataHash();
            hash += centerTransform.GetDataHash();
            hash += springData.GetDataHash();
            return hash;
        }

        //=========================================================================================
        public VirtualMeshDeformer Deformer
        {
            get
            {
                if (virtualDeformer != null)
                    return virtualDeformer.Deformer;
                return null;
            }
        }

        public SpringData SpringData
        {
            get
            {
                return springData;
            }
        }

        public int UseVertexCount
        {
            get
            {
                if (springData == null)
                    return 0;
                else
                    return springData.UseVertexCount;
            }
        }

        public Transform CenterTransform
        {
            get
            {
                return centerTransform;
            }
            set
            {
                centerTransform = value;
            }
        }

        public Axis DirectionAxis
        {
            get
            {
                return directionAxis;
            }
            set
            {
                directionAxis = value;
            }
        }

        public Vector3 CenterTransformDirection
        {
            get
            {
                Vector3 dir = Vector3.forward;
                if (centerTransform)
                {
                    switch (directionAxis)
                    {
                        case Axis.X:
                            dir = centerTransform.right;
                            break;
                        case Axis.Y:
                            dir = centerTransform.up;
                            break;
                        case Axis.Z:
                            dir = centerTransform.forward;
                            break;
                        case Axis.InverseX:
                            dir = -centerTransform.right;
                            break;
                        case Axis.InverseY:
                            dir = -centerTransform.up;
                            break;
                        case Axis.InverseZ:
                            dir = -centerTransform.forward;
                            break;
                    }
                }

                return dir;
            }
        }

        public SpringData.DeformerData GetDeformerData()
        {
            return springData.deformerData;
        }

        //=========================================================================================
        protected override void Reset()
        {
            base.Reset();
            ResetParams();
        }

        protected override void OnValidate()
        {
            base.OnValidate();
        }

        //=========================================================================================
        /// <summary>
        /// クロス初期化
        /// </summary>
        protected override void ClothInit()
        {
            // 中央トランスフォームに移動パーティクルを１つ設定する（これが揺れる）
            // クロスデータはこの場で作成する
            ClothData cdata = ShareDataObject.CreateShareData<ClothData>("ClothData_work");
            cdata.selectionData.Add(SelectionData.Move);
            cdata.vertexFlagLevelList.Add(0);
            cdata.vertexDepthList.Add(0);
            cdata.rootList.Add(0);
            cdata.useVertexList.Add(0);
            cdata.SaveDataHash = 1;
            cdata.SaveDataVersion = cdata.GetVersion();
            clothData = cdata;

            // エラーが出ないように
            clothDataHash = cdata.SaveDataHash;
            clothDataVersion = cdata.SaveDataVersion;

            // クロス初期化
            base.ClothInit();
        }

        /// <summary>
        /// 頂点ごとのパーティクルフラグ設定（不要な場合は０）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override uint UserFlag(int index)
        {
            uint flag = 0;
            flag |= PhysicsManagerParticleData.Flag_Transform_Read_Base; // トランスフォームをbasePos/baseRotに読み込み
            flag |= PhysicsManagerParticleData.Flag_Transform_Read_Rot; // トランスフォームをrotに読み込む
            return flag;
        }

        /// <summary>
        /// 頂点ごとの連動トランスフォーム設定（不要な場合はnull）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override Transform UserTransform(int index)
        {
            return CenterTransform;
        }

        /// <summary>
        /// 頂点ごとの連動トランスフォームのLocalPositionを返す（不要な場合は0）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override float3 UserTransformLocalPosition(int vindex)
        {
            return CenterTransform.localPosition;
        }

        /// <summary>
        /// 頂点ごとの連動トランスフォームのLocalRotationを返す（不要な場合はquaternion.identity)
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected override quaternion UserTransformLocalRotation(int vindex)
        {
            return CenterTransform.localRotation;
        }

        /// <summary>
        /// デフォーマーの数を返す
        /// </summary>
        /// <returns></returns>
        public override int GetDeformerCount()
        {
            return (virtualDeformer != null) ? 1 : 0;
        }

        /// <summary>
        /// デフォーマーを返す
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override BaseMeshDeformer GetDeformer(int index)
        {
            if (virtualDeformer != null)
            {
                return virtualDeformer.Deformer;
            }
            return null;
        }

        /// <summary>
        /// クロス初期化時に必要なMeshDataを返す（不要ならnull）
        /// </summary>
        /// <returns></returns>
        protected override MeshData GetMeshData()
        {
            // MeshSpringeには不要
            return null;
        }

        /// <summary>
        /// クロス初期化の主にワーカーへの登録
        /// </summary>
        protected override void WorkerInit()
        {
            // センターパーティクル
            int pindex = ParticleChunk.startIndex;

            // デフォーマーごとの設定
            SpringMeshWorker worker = MagicaPhysicsManager.Instance.Compute.SpringMeshWorker;
            {
                // デフォーマー取得
                var deformer = GetDeformer(0);
                Debug.Assert(deformer != null);
                deformer.Init();

                // スプリングデータ取得
                var data = GetDeformerData();
                Debug.Assert(data != null);

                // スプリングワーカー設定
                var minfo = MagicaPhysicsManager.Instance.Mesh.GetVirtualMeshInfo(deformer.MeshIndex);
                for (int j = 0; j < data.UseVertexCount; j++)
                {
                    int vindex = data.useVertexIndexList[j];
                    worker.Add(TeamId, minfo.vertexChunk.startIndex + vindex, pindex, data.weightList[j]);
                }
            }
        }

        /// <summary>
        /// デフォーマーごとの使用頂点設定
        /// 使用頂点に対して AddUseVertex() / RemoveUseVertex() を実行する
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="deformer"></param>
        /// <param name="deformerIndex"></param>
        protected override void SetDeformerUseVertex(bool sw, BaseMeshDeformer deformer, int deformerIndex)
        {
            var data = GetDeformerData();

            int vcnt = data.UseVertexCount;
            for (int j = 0; j < vcnt; j++)
            {
                int vindex = data.useVertexIndexList[j];
                if (sw)
                    deformer.AddUseVertex(vindex);
                else
                    deformer.RemoveUseVertex(vindex);
            }
        }

        //=========================================================================================
        public override int GetVersion()
        {
            return DATA_VERSION;
        }

        /// <summary>
        /// データを検証して結果を格納する
        /// </summary>
        /// <returns></returns>
        public override void CreateVerifyData()
        {
            base.CreateVerifyData();
            virtualDeformerHash = virtualDeformer.SaveDataHash;
            virtualDeformerVersion = virtualDeformer.SaveDataVersion;
            springDataHash = springData.SaveDataHash;
            springDataVersion = springData.SaveDataVersion;
        }

        /// <summary>
        /// 現在のデータが正常（実行できる状態）か返す
        /// </summary>
        /// <returns></returns>
        public override bool VerifyData()
        {
            if (base.VerifyData() == false)
                return false;

            if (virtualDeformer == null)
                return false;
            if (virtualDeformer.VerifyData() == false)
                return false;
            if (virtualDeformerHash != virtualDeformer.SaveDataHash)
                return false;
            if (virtualDeformerVersion != virtualDeformer.SaveDataVersion)
                return false;

            if (centerTransform == null)
                return false;
            if (springData == null)
                return false;
            if (springData.VerifyData() == false)
                return false;
            if (springDataHash != springData.SaveDataHash)
                return false;
            if (springDataVersion != springData.SaveDataVersion)
                return false;

            return true;
        }

        public override string GetInformation()
        {
            StaticStringBuilder.Clear();

            if (VerifyData())
            {
                // OK
                StaticStringBuilder.Append("Use Deformer Vertex: ", UseVertexCount);
            }
            else
            {
                // エラー
                StaticStringBuilder.AppendLine("This mesh spring is in a state error!");
                if (Application.isPlaying)
                {
                    StaticStringBuilder.Append("Execution stopped.");
                }
                else
                {
                    StaticStringBuilder.Append("Please recreate the mesh spring data.");
                }
            }

            return StaticStringBuilder.ToString();
        }

        /// <summary>
        /// デフォーマーの検証
        /// </summary>
        public void VerifyDeformer()
        {
        }

        //=========================================================================================
        /// <summary>
        /// メッシュのワールド座標/法線/接線を返す（エディタ用）
        /// </summary>
        /// <param name="wposList"></param>
        /// <param name="wnorList"></param>
        /// <param name="wtanList"></param>
        /// <returns>頂点数</returns>
        public override int GetEditorPositionNormalTangent(out List<Vector3> wposList, out List<Vector3> wnorList, out List<Vector3> wtanList)
        {
            wposList = new List<Vector3>();
            wnorList = new List<Vector3>();
            wtanList = new List<Vector3>();

            var t = CenterTransform;
            if (t == null)
                return 0;

            wposList.Add(t.position);
            wnorList.Add(t.forward);
            var up = t.up;
            wtanList.Add(up);

            return 1;
        }

        /// <summary>
        /// メッシュのトライアングルリストを返す（エディタ用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorTriangleList()
        {
            return null;
        }

        /// <summary>
        /// メッシュのラインリストを返す（エディタ用）
        /// </summary>
        /// <returns></returns>
        public override List<int> GetEditorLineList()
        {
            return null;
        }

        //=========================================================================================
        /// <summary>
        /// 頂点の選択状態をリストにして返す（エディタ用）
        /// 選択状態は ClothSelection.Invalid / ClothSelection.Fixed / ClothSelection.Move
        /// すべてがInvalidならばnullを返す
        /// </summary>
        /// <returns></returns>
        public override List<int> GetSelectionList()
        {
            return null;
        }

        /// <summary>
        /// 頂点の使用状態をリストにして返す（エディタ用）
        /// 数値が１以上ならば使用中とみなす
        /// すべて使用状態ならばnullを返す
        /// </summary>
        /// <returns></returns>
        public override List<int> GetUseList()
        {
            return null;
        }


        //=========================================================================================
        /// <summary>
        /// 共有データオブジェクト収集
        /// </summary>
        /// <returns></returns>
        public override List<ShareDataObject> GetAllShareDataObject()
        {
            var sdata = base.GetAllShareDataObject();
            sdata.Add(SpringData);
            return sdata;
        }

        //=========================================================================================
        /// <summary>
        /// パラメータ初期化
        /// </summary>
        void ResetParams()
        {
            clothParams.SetRadius(0.02f, 0.02f);
            clothParams.SetMass(1.0f, 1.0f, false);
            clothParams.SetGravity(false, -9.8f, -9.8f);
            clothParams.SetDrag(true, 0.01f, 0.01f);
            clothParams.SetMaxVelocity(true, 3.0f, 3.0f);
            clothParams.SetWorldInfluence(0.5f, 0.5f);
            clothParams.SetTeleport(true);
            clothParams.SetClampDistanceRatio(false);
            clothParams.SetClampPositionLength(true, 0.1f, 0.1f, 1.0f, 1.0f, 1.0f, 0.5f);
            clothParams.SetClampRotationAngle(false);
            clothParams.SetRestoreDistance(1.0f);
            clothParams.SetRestoreRotation(false);
            clothParams.SetSpring(true, 0.02f, 0.14f, 1.0f, 1.0f, 1.0f, 1.0f);
            clothParams.SetSpringDirectionAtten(1.0f, 0.0f, 0.6f);
            clothParams.SetSpringDistanceAtten(1.0f, 0.0f, 0.4f);
            clothParams.SetAdjustRotation(false, ClothParams.AdjustMode.None, 5.0f);
            clothParams.SetTriangleBend(false);
            clothParams.SetVolume(false);
            clothParams.SetCollision(false, 0.2f);
        }
    }
}
