﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// クロス選択データ
    /// </summary>
    [System.Serializable]
    public class SelectionData : ShareDataObject
    {
        /// <summary>
        /// データバージョン
        /// </summary>
        private const int DATA_VERSION = 2;

        /// <summary>
        /// 頂点選択データタイプ
        /// </summary>
        public const int Invalid = 0;
        public const int Move = 1;
        public const int Fixed = 2;
        public const int Extend = 3; // 固定としてマークするがローテーションライン計算からは除外する

        /// <summary>
        /// レンダーデフォーマーごとの選択データ
        /// </summary>
        [System.Serializable]
        public class DeformerSelection : IDataHash
        {
            /// <summary>
            /// レンダーデフォーマーの頂点と１対１に対応
            /// </summary>
            public List<int> selectData = new List<int>();

            public int GetDataHash()
            {
                return selectData.GetDataHash();
            }

            public bool Compare(DeformerSelection data)
            {
                if (selectData.Count != data.selectData.Count)
                    return false;
                for (int i = 0; i < selectData.Count; i++)
                {
                    if (selectData[i] != data.selectData[i])
                        return false;
                }

                return true;
            }
        }
        public List<DeformerSelection> selectionList = new List<DeformerSelection>();

        //=========================================================================================
        public int DeformerCount
        {
            get
            {
                return selectionList.Count;
            }
        }

        //=========================================================================================
        /// <summary>
        /// データハッシュ計算
        /// </summary>
        /// <returns></returns>
        public override int GetDataHash()
        {
            int hash = 0;
            hash += selectionList.GetDataHash();
            return hash;
        }

        //=========================================================================================
        public override int GetVersion()
        {
            return DATA_VERSION;
        }

        /// <summary>
        /// 現在のデータが正常（実行できる状態）か返す
        /// </summary>
        /// <returns></returns>
        public override bool VerifyData()
        {
            if (dataHash == 0)
                return false;
            if (dataVersion != GetVersion())
                return false;

            if (selectionList.Count == 0)
                return false;

            return true;
        }

        //=========================================================================================
        /// <summary>
        /// 引数の選択データの内容を比較する
        /// </summary>
        /// <param name="sel"></param>
        /// <returns></returns>
        public bool Compare(SelectionData sel)
        {
            if (selectionList.Count != sel.selectionList.Count)
                return false;

            for (int i = 0; i < selectionList.Count; i++)
            {
                if (selectionList[i].Compare(sel.selectionList[i]) == false)
                    return false;
            }

            return true;
        }


        /// <summary>
        /// メッシュデータの各頂点の選択情報を取得する
        /// </summary>
        /// <param name="meshData"></param>
        /// <returns></returns>
        public List<int> GetSelectionData(MeshData meshData)
        {
            List<int> selects = new List<int>();

            if (meshData != null)
            {
                // 親頂点に影響する子頂点情報
                Dictionary<int, List<uint>> dict = meshData.GetVirtualToChildVertexDict();

                int vcnt = meshData.VertexCount;
                for (int i = 0; i < vcnt; i++)
                {
                    int data = GetSelection(meshData, i, dict);
                    selects.Add(data);
                }
            }
            else
            {
                // そのまま
                if (selectionList.Count > 0)
                {
                    selects = new List<int>(selectionList[0].selectData);
                }
            }

            return selects;
        }

        /// <summary>
        /// メッシュデータの指定インデクスの選択情報を取得する
        /// </summary>
        /// <param name="meshData"></param>
        /// <param name="vindex"></param>
        /// <returns></returns>
        private int GetSelection(MeshData meshData, int vindex, Dictionary<int, List<uint>> dict)
        {
            int data = Invalid;

            // セレクションデータ読み込み
            if (meshData != null && meshData.ChildCount > 0)
            {
                // 親頂点に影響する子頂点情報から取得
                if (dict.ContainsKey(vindex))
                {
                    foreach (var pack in dict[vindex])
                    {
                        int cmindex = DataUtility.Unpack16Hi(pack);
                        int cvindex = DataUtility.Unpack16Low(pack);

                        if (cmindex < selectionList.Count && cvindex < selectionList[cmindex].selectData.Count)
                        {
                            data = Mathf.Max(selectionList[cmindex].selectData[cvindex], data);
                        }
                    }
                }
            }
            else
            {
                // そのまま
                int dindex = 0;
                if (dindex < selectionList.Count)
                {
                    if (vindex < selectionList[dindex].selectData.Count)
                        data = selectionList[dindex].selectData[vindex];
                }
            }

            return data;
        }

        /// <summary>
        /// メッシュ頂点の選択データを設定する
        /// </summary>
        /// <param name="meshData"></param>
        /// <param name="selects"></param>
        public void SetSelectionData(MeshData meshData, List<int> selects)
        {
            // 選択データ初期化
            selectionList.Clear();
            if (meshData != null && meshData.ChildCount > 0)
            {
                for (int i = 0; i < meshData.ChildCount; i++)
                {
                    var dsel = new DeformerSelection();
                    int cvcnt = meshData.childDataList[i].VertexCount;
                    for (int j = 0; j < cvcnt; j++)
                        dsel.selectData.Add(Invalid);

                    selectionList.Add(dsel);
                }
            }
            else
            {
                // そのまま
                var dsel = new DeformerSelection();
                int cvcnt = selects.Count;
                for (int j = 0; j < cvcnt; j++)
                    dsel.selectData.Add(Invalid);

                selectionList.Add(dsel);
            }

            // 選択データに追加
            for (int i = 0; i < selects.Count; i++)
            {
                int data = selects[i];
                if (meshData != null && meshData.ChildCount > 0)
                {
                    // 親頂点に影響する子頂点情報
                    Dictionary<int, List<uint>> dict = meshData.GetVirtualToChildVertexDict();

                    // 親頂点に影響する子頂点に記録
                    if (dict.ContainsKey(i))
                    {
                        foreach (var pack in dict[i])
                        {
                            int cmindex = DataUtility.Unpack16Hi(pack);
                            int cvindex = DataUtility.Unpack16Low(pack);

                            selectionList[cmindex].selectData[cvindex] = data;
                        }
                    }
                }
                else
                {
                    // そのまま
                    selectionList[0].selectData[i] = data;
                }
            }

            // データハッシュ設定
            CreateVerifyData();
        }
    }
}
