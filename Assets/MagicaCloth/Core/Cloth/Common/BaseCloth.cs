﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// クロス基本クラス
    /// </summary>
    public abstract partial class BaseCloth : PhysicsTeam
    {
        /// <summary>
        /// パラメータ設定
        /// </summary>
        [SerializeField]
        protected ClothParams clothParams = new ClothParams();

        /// <summary>
        /// クロスデータ
        /// </summary>
        [SerializeField]
        protected ClothData clothData = null;

        [SerializeField]
        protected int clothDataHash;
        [SerializeField]
        protected int clothDataVersion;

        /// <summary>
        /// 頂点選択データ
        /// </summary>
        [SerializeField]
        protected SelectionData clothSelection = null;

        [SerializeField]
        private int clothSelectionHash;
        [SerializeField]
        private int clothSelectionVersion;

        /// <summary>
        /// ランタイムクロス設定
        /// </summary>
        protected ClothSetup setup = new ClothSetup();

        //=========================================================================================
        /// <summary>
        /// データハッシュを求める
        /// </summary>
        /// <returns></returns>
        public override int GetDataHash()
        {
            int hash = base.GetDataHash();
            if (clothData != null)
                hash += clothData.GetDataHash();
            if (clothSelection != null)
                hash += clothSelection.GetDataHash();

            return hash;
        }

        //=========================================================================================
        public ClothParams Params
        {
            get
            {
                return clothParams;
            }
        }

        public ClothData ClothData
        {
            get
            {
                return clothData;
            }
        }

        public SelectionData ClothSelection
        {
            get
            {
                return clothSelection;
            }
        }

        public ClothSetup Setup
        {
            get
            {
                return setup;
            }
        }

        //=========================================================================================
        protected virtual void Reset()
        {
        }

        protected virtual void OnValidate()
        {
            if (Application.isPlaying == false)
                return;

            // クロスパラメータのラインタイム変更
            setup.ChangeData(this, clothParams);
        }

        //=========================================================================================
        protected override void OnInit()
        {
            base.OnInit();
            BaseClothInit();
        }

        protected override void OnActive()
        {
            base.OnActive();
            // パーティクル有効化
            EnableParticle(UserTransform, UserTransformLocalPosition, UserTransformLocalRotation);
            SetUseMesh(true);
            ClothActive();
        }

        protected override void OnInactive()
        {
            base.OnInactive();
            // パーティクル無効化
            DisableParticle(UserTransform, UserTransformLocalPosition, UserTransformLocalRotation);
            SetUseMesh(false);
            ClothInactive();
        }

        protected override void OnDispose()
        {
            BaseClothDispose();
            base.OnDispose();
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
        }

        //=========================================================================================
        void BaseClothInit()
        {
            // デフォーマー初期化
            int dcount = GetDeformerCount();
            for (int i = 0; i < dcount; i++)
            {
                var deformer = GetDeformer(i);
                if (deformer == null)
                {
                    Status.SetInitError();
                    return;
                }
                deformer.Init();
                if (deformer.Status.IsInitError)
                {
                    Status.SetInitError();
                    return;
                }
            }

            if (VerifyData() == false)
            {
                Status.SetInitError();
                return;
            }

            // クロス初期化
            ClothInit();

            // クロス初期化後の主にワーカーへの登録
            WorkerInit();

            // 頂点有効化
            SetUseVertex(true);
        }

        /// <summary>
        /// クロス初期化
        /// </summary>
        protected virtual void ClothInit()
        {
            //setup.ClothInit(this, GetMeshData(), clothData, clothParams, UserFlag, UserTransform, UserTransformLocalPosition, UserTransformLocalRotation);
            setup.ClothInit(this, GetMeshData(), clothData, clothParams, UserFlag);
        }

        protected virtual void ClothActive()
        {
            setup.ClothActive(this, clothParams);
        }

        protected virtual void ClothInactive()
        {
            setup.ClothInactive(this);
        }

        /// <summary>
        /// 頂点ごとのパーティクルフラグ設定（不要な場合は０）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected abstract uint UserFlag(int vindex);

        /// <summary>
        /// 頂点ごとの連動トランスフォーム設定（不要な場合はnull）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected abstract Transform UserTransform(int vindex);

        /// <summary>
        /// 頂点ごとの連動トランスフォームのLocalPositionを返す（不要な場合は0）
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected abstract float3 UserTransformLocalPosition(int vindex);

        /// <summary>
        /// 頂点ごとの連動トランスフォームのLocalRotationを返す（不要な場合はquaternion.identity)
        /// </summary>
        /// <param name="vindex"></param>
        /// <returns></returns>
        protected abstract quaternion UserTransformLocalRotation(int vindex);

        /// <summary>
        /// デフォーマーの数を返す
        /// </summary>
        /// <returns></returns>
        public abstract int GetDeformerCount();

        /// <summary>
        /// デフォーマーを返す
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public abstract BaseMeshDeformer GetDeformer(int index);

        /// <summary>
        /// クロス初期化時に必要なMeshDataを返す（不要ならnull）
        /// </summary>
        /// <returns></returns>
        protected abstract MeshData GetMeshData();

        /// <summary>
        /// クロス初期化後の主にワーカーへの登録
        /// </summary>
        protected abstract void WorkerInit();


        //=========================================================================================
        void BaseClothDispose()
        {
            if (MagicaPhysicsManager.IsInstance() == false)
                return;

            if (Status.IsInitSuccess)
            {
                // 頂点無効化
                SetUseVertex(false);

                // クロス破棄
                // この中ですべてのコンストレイントとワーカーからチームのデータが自動削除される
                setup.ClothDispose(this);
            }
        }

        //=========================================================================================
        /// <summary>
        /// 使用デフォーマー設定
        /// </summary>
        /// <param name="sw"></param>
        void SetUseMesh(bool sw)
        {
            if (MagicaPhysicsManager.IsInstance() == false)
                return;

            if (Status.IsInitSuccess == false)
                return;

            int dcount = GetDeformerCount();
            for (int i = 0; i < dcount; i++)
            {
                var deformer = GetDeformer(i);
                if (deformer != null)
                {
                    if (sw)
                        deformer.AddUseMesh(this);
                    else
                        deformer.RemoveUseMesh(this);
                }
            }
        }

        /// <summary>
        /// 使用頂点設定
        /// </summary>
        /// <param name="sw"></param>
        void SetUseVertex(bool sw)
        {
            if (MagicaPhysicsManager.IsInstance() == false)
                return;

            int dcount = GetDeformerCount();
            for (int i = 0; i < dcount; i++)
            {
                var deformer = GetDeformer(i);
                if (deformer != null)
                {
                    SetDeformerUseVertex(sw, deformer, i);
                }
            }
        }

        /// <summary>
        /// デフォーマーごとの使用頂点設定
        /// 使用頂点に対して AddUseVertex() / RemoveUseVertex() を実行する
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="deformer"></param>
        /// <param name="deformerIndex"></param>
        protected abstract void SetDeformerUseVertex(bool sw, BaseMeshDeformer deformer, int deformerIndex);

        //=========================================================================================
        /// <summary>
        /// データを検証して結果を格納する
        /// </summary>
        /// <returns></returns>
        public override void CreateVerifyData()
        {
            base.CreateVerifyData();
            clothDataHash = clothData != null ? clothData.SaveDataHash : 0;
            clothDataVersion = clothData != null ? clothData.SaveDataVersion : 0;
            clothSelectionHash = clothSelection != null ? clothSelection.SaveDataHash : 0;
            clothSelectionVersion = clothSelection != null ? clothSelection.SaveDataVersion : 0;
        }

        /// <summary>
        /// 現在のデータが正常（実行できる状態）か返す
        /// </summary>
        /// <returns></returns>
        public override bool VerifyData()
        {
            if (base.VerifyData() == false)
                return false;

            // clothDataはオプション
            if (clothData != null)
            {
                if (clothData.VerifyData() == false)
                    return false;
                if (clothDataHash != clothData.SaveDataHash)
                    return false;
                if (clothDataVersion != clothData.SaveDataVersion)
                    return false;
            }

            // clothSelectionはオプション
            if (clothSelection != null)
            {
                if (clothSelection.VerifyData() == false)
                    return false;
                if (clothSelectionHash != clothSelection.SaveDataHash)
                    return false;
                if (clothSelectionVersion != clothSelection.SaveDataVersion)
                    return false;
            }

            return true;
        }

        //=========================================================================================
        /// <summary>
        /// 共有データオブジェクト収集
        /// </summary>
        /// <returns></returns>
        public override List<ShareDataObject> GetAllShareDataObject()
        {
            var sdata = base.GetAllShareDataObject();
            sdata.Add(clothData);
            sdata.Add(clothSelection);
            return sdata;
        }
    }
}
