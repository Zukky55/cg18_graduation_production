﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;

namespace MagicaCloth
{
    /// <summary>
    /// コンテンツの実行状態を管理する
    /// </summary>
    public class RuntimeStatus
    {
        // 以下は現在の状態フラグ
        /// <summary>
        /// 初期化が完了するとtrueになる（エラーの有無は問わない）
        /// </summary>
        bool init;

        /// <summary>
        /// 初期化エラーが発生するとtrueになる
        /// </summary>
        bool initError;

        /// <summary>
        /// コンテンツの有効状態の切り替え
        /// </summary>
        bool enable;

        /// <summary>
        /// 実行中にエラーが発生した場合にtrueになる
        /// </summary>
        bool runtimeError;

        /// <summary>
        /// コンテンツが破棄された場合にtrueとなる
        /// </summary>
        bool dispose;

        /// <summary>
        /// コンテンツの現在の稼働状態
        /// </summary>
        bool isActive;

        /// <summary>
        /// 連動ステータス
        /// 設定されている場合、こららのステータスがすべて停止中ならば自身も停止する
        /// </summary>
        List<RuntimeStatus> linkStatusList = new List<RuntimeStatus>();

        //=========================================================================================
        /// <summary>
        /// 現在稼働中か判定する
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isActive && !dispose;
            }
        }

        /// <summary>
        /// 初期化済みか判定する（エラーの有無は問わない）
        /// </summary>
        public bool IsInitComplete
        {
            get
            {
                return init;
            }
        }

        /// <summary>
        /// 初期化が成功しているか判定する
        /// </summary>
        public bool IsInitSuccess
        {
            get
            {
                return init && !initError;
            }
        }

        /// <summary>
        /// 初期化が失敗しているか判定する
        /// </summary>
        public bool IsInitError
        {
            get
            {
                return init && initError;
            }
        }

        /// <summary>
        /// 初期化済みフラグを立てる
        /// </summary>
        public void SetInitComplete()
        {
            init = true;
        }

        /// <summary>
        /// 初期化エラーフラグを立てる
        /// </summary>
        public void SetInitError()
        {
            initError = true;
        }

        /// <summary>
        /// 有効フラグを設定する
        /// </summary>
        /// <param name="sw"></param>
        /// <returns>フラグに変更があった場合はtrueが返る</returns>
        public bool SetEnable(bool sw)
        {
            bool ret = enable != sw;
            enable = sw;
            return ret;
        }

        /// <summary>
        /// ランタイムエラーフラグを設定する
        /// </summary>
        /// <param name="sw"></param>
        /// <returns>フラグに変更があった場合はtrueが返る</returns>
        public bool SetRuntimeError(bool sw)
        {
            bool ret = runtimeError != sw;
            runtimeError = sw;
            return ret;
        }

        /// <summary>
        /// 破棄フラグを立てる
        /// </summary>
        /// <returns></returns>
        public void SetDispose()
        {
            dispose = true;
        }

        /// <summary>
        /// 現在のアクティブ状態を更新する
        /// </summary>
        /// <returns>アクティブ状態に変更があった場合はtrueが返る</returns>
        public bool UpdateStatus()
        {
            if (dispose)
                return false;

            // 初期化済み、有効状態、エラー状態、連動ステータス状態、がすべてクリアならばアクティブ状態と判定
            var active = init && !initError && enable && !runtimeError && IsLinkStatusActive();
            if (active != isActive)
            {
                isActive = active;
                return true;
            }
            else
                return false;
        }

        //=========================================================================================
        /// <summary>
        /// 連動ステータスを追加する
        /// </summary>
        /// <param name="status"></param>
        public void AddLinkStatus(RuntimeStatus status)
        {
            linkStatusList.Add(status);
        }

        /// <summary>
        /// 連動ステータスを削除する
        /// </summary>
        /// <param name="status"></param>
        public void RemoveLinkStatus(RuntimeStatus status)
        {
            linkStatusList.Remove(status);
        }

        /// <summary>
        /// 連動ステータスが１つでも稼働状態か調べる
        /// 連動ステータスが無い場合は稼働状態として返す
        /// </summary>
        /// <returns></returns>
        bool IsLinkStatusActive()
        {
            if (linkStatusList.Count == 0)
                return true;

            foreach (var status in linkStatusList)
                if (status.IsActive)
                    return true;

            return false;
        }
    }
}
