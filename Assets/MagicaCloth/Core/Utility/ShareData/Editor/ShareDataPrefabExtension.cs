﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.SceneManagement;
using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// 共有データオブジェクトのプレハブ化処理
    /// プレハブがApplyされた場合に、自動でスクリプタブルオブジェクをプレハブのサブアセットとして保存します。
    /// 該当するコンポーネントにIShareDataObjectを継承し、GetAllShareDataObject()で該当する共有データを返す必要があります。
    /// </summary>
    [InitializeOnLoad]
    internal class ShareDataPrefabExtension
    {
        /// <summary>
        /// プレハブ更新コールバック登録
        /// </summary>
        static ShareDataPrefabExtension()
        {
            PrefabUtility.prefabInstanceUpdated += OnPrefabInstanceUpdate;

            //PrefabStage.prefabStageOpened += OnPrefabStageOpened;
            //PrefabStage.prefabStageClosing += OnPrefabStageClosing;
            //PrefabStage.prefabSaving += OnPrefabSaving;
            //PrefabStage.prefabSaved += OnPrefabSaved;
        }

#if false
        static void OnPrefabStageOpened(PrefabStage pstage)
        {
            Debug.Log("プレハブステージオープン->" + pstage.prefabAssetPath);
        }

        static void OnPrefabStageClosing(PrefabStage pstage)
        {
            Debug.Log("プレハブステージクローズ->" + pstage.prefabAssetPath);

            // オートセーブをOFFにした場合があるので、これはまずい
            //GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(pstage.prefabAssetPath);
            //PrefabUpdate(prefab, pstage.prefabContentsRoot, pstage.prefabAssetPath);
        }

        static void OnPrefabSaving(GameObject obj)
        {
            Debug.Log("プレハブ保存->" + obj.name);
        }

        static void OnPrefabSaved(GameObject obj)
        {
            Debug.Log("プレハブ保存2->" + obj.name);
        }
#endif

        /// <summary>
        /// プレハブがApplyされた場合に呼ばれる
        /// instanceはヒエラルキーにあるゲームオブジェクト
        /// プレハブが更新された場合、スクリプタブルオブジェクをプレハブのサブアセットとして自動保存する
        /// </summary>
        /// <param name="instance"></param>
        static void OnPrefabInstanceUpdate(GameObject instance)
        {
            //Debug.Log("プレハブApply ->" + instance.name);

            GameObject prefab = PrefabUtility.GetCorrespondingObjectFromSource(instance) as GameObject;
            string prefabPath = AssetDatabase.GetAssetPath(prefab);
            //Debug.Log("Prefab originPath=" + prefabPath);

            // プレハブモード判定
            var pstage = PrefabStageUtility.GetCurrentPrefabStage();
            bool prefabMode = pstage != null;
            //if (prefabMode)
            //    return;

            GameObject target = prefabMode ? pstage.prefabContentsRoot : instance;

            PrefabUpdate(prefab, target, prefabPath);
        }

        static void PrefabUpdate(GameObject prefab, GameObject target, string prefabPath)
        {
            //Debug.Log("PrefabUpdate()");

            // 不要な共有データを削除するためのリスト
            bool change = false;
            List<ShareDataObject> removeDatas = new List<ShareDataObject>();

            // 現在アセットとして保存されているすべてのShareDataObjectサブアセットを削除対象としてリスト化する
            UnityEngine.Object[] subassets = AssetDatabase.LoadAllAssetRepresentationsAtPath(prefabPath);
            if (subassets != null)
            {
                foreach (var obj in subassets)
                {
                    // ShareDataObjectのみ
                    ShareDataObject sdata = obj as ShareDataObject;
                    if (sdata)
                    {
                        //Debug.Log("サブ:" + obj.name + " type:" + obj + " test:" + AssetDatabase.IsSubAsset(sdata));

                        // 削除対象として一旦追加
                        removeDatas.Add(sdata);
                    }
                }
            }

            // プレハブ元の共有オブジェクトをサブアセットとして保存する
            //var target = prefabMode ? pstage.prefabContentsRoot : instance;
            var shareDataInterfaces = target.GetComponentsInChildren<IShareDataObject>(true);
            if (shareDataInterfaces != null)
            {
                foreach (var sdataInterface in shareDataInterfaces)
                {
                    List<ShareDataObject> shareDatas = sdataInterface.GetAllShareDataObject();
                    if (shareDatas != null)
                    {
                        foreach (var sdata in shareDatas)
                        {
                            if (sdata)
                            {
                                //Debug.Log("共有データ->" + sdata.name + " プレハブ?:" + AssetDatabase.Contains(sdata));

                                if (AssetDatabase.Contains(sdata) == false)
                                {
                                    // サブアセットとして共有データを追加
                                    //Debug.Log("サブアセットとして保存");
                                    AssetDatabase.AddObjectToAsset(sdata, prefab);
                                    change = true;
                                }
                                else
                                {
                                    // 削除対象から除外
                                    removeDatas.Remove(sdata);
                                }
                            }
                        }
                    }
                }
            }

            // 不要な共有データは削除する
            foreach (var sdata in removeDatas)
            {
                UnityEngine.Object.DestroyImmediate(sdata, true);
                change = true;
            }

            // 変更を全体に反映
            if (change)
            {
                //Debug.Log("保存!");

                // どうもこの手順を踏まないと保存した共有データが正しくアタッチされない
                bool success;
                PrefabUtility.SaveAsPrefabAssetAndConnect(target, prefabPath, InteractionMode.AutomatedAction, out success);
                if (success == false)
                    Debug.LogError("Failed to save magicacloth shared data.");
            }
        }
    }
}
