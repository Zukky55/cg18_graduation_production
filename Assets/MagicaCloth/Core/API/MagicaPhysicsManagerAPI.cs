﻿// Magica Cloth.
// Copyright (c) MagicaSoft, 2020.
// https://magicasoft.jp

using UnityEngine;

namespace MagicaCloth
{
    /// <summary>
    /// 物理マネージャAPI
    /// </summary>
    public partial class MagicaPhysicsManager : CreateSingleton<MagicaPhysicsManager>
    {
        /// <summary>
        /// グローバルタイムスケールを設定する
        /// </summary>
        /// <param name="timeScale">0.0-1.0</param>
        public void SetGlobalTimeScale(float timeScale)
        {
            UpdateTime.TimeScale = Mathf.Clamp01(timeScale);
        }

        /// <summary>
        /// グローバルタイムスケールを取得する
        /// </summary>
        /// <returns></returns>
        public float GetGlobalTimeScale()
        {
            return UpdateTime.TimeScale;
        }
    }
}
