﻿using UnityEngine;

public class SyncCustomPropertiesAnimator : MonoBehaviour
{

    [SerializeField, Header("値を設定したいSkinnedMeshRenderer")]
    SkinnedMeshRenderer _renderer;

    [SerializeField, Header("アニメーションプロパティ名 U")]
    string animationPropertyName_U;
    [SerializeField, Header("アニメーションプロパティ名 V")]
    string animationPropertyName_V;

    Animator animator;


    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        Vector2 value = new Vector2(animator.GetFloat(animationPropertyName_U), animator.GetFloat(animationPropertyName_V));
        _renderer.material.SetTextureOffset("_MainTex", value);
        _renderer.material.SetTextureOffset("_ShadowTex", value);
        _renderer.material.SetTextureOffset("_HighlightTex", value);

        //Debug.Log($"{"_MainTex"} = {_renderer.material.GetTextureOffset("_MainTex")}");
        //Debug.Log($"{"_ShadowTex"} = {_renderer.material.GetTextureOffset("_ShadowTex")}");
    }
}