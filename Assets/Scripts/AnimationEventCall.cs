﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public enum EffectType
{
    Fireeee,
    Waterrrrr,
    Thunder,
}

public class AnimationEventCall : MonoBehaviour
{
    [SerializeField] EffectDatas effectDatas;
    [SerializeField] List<EffectParam> effectParams;
    List<GameObject> tempEffects;
    private void Start()
    {
        CreateEffects();
    }

    /// <summary>エフェクトを指定数生成</summary>
    void CreateEffects()
    {
        tempEffects = new List<GameObject>();

        effectDatas.effectDatas.ForEach(effect =>
        {
            for(int i = 0; i < effect.generateNumber; i++)
            {
                var temp = Instantiate(effect.effectParam.effectObj);
                temp.SetActive(false);
                temp.name = $"{effect.effectParam.effectType}{i}";
                tempEffects.Add(temp);
            }
            Debug.Log($"{effect.effectParam.effectType}を{effect.generateNumber}個生成");
        });
        //effectParams.ForEach(effect =>
        //{
        //    var temp = Instantiate(effect.effectObj);
        //    temp.SetActive(false);
        //    temp.name = $"{effect.effectType}{createCount}";
        //    createCount++;
        //    tempEffects.Add(temp);
        //});
    }

    /// <summary>エフェクトを発生させる</summary>
    /// <param name="effectParam"></param>
    /// <param name="lifeTime"></param>
    public void PlayEffect(EffectParam effectParam)
    {
        var effect = tempEffects.Find(ef => ef.name == effectParam.effectType.ToString() + effectParam.number.ToString());
        if(effect != null && !effect.activeSelf)
        {
            effect.transform.position = effectParam.position;
            effect.transform.rotation = effectParam.quaternion;
            effect.SetActive(true);
            Debug.Log($"{effect.name}を再生");
        }
        else
        {
            Debug.LogError("再生するエフェクトがないわ！");
        }
    }

    public void DestroyEffect(EffectParam effectParam)
    {
        var effect = tempEffects.Find(ef => ef.name == effectParam.effectType.ToString() + effectParam.number.ToString());
        if(effect != null && effect.activeSelf)
        {
            effect.SetActive(false);
            Debug.Log($"{effect.name}を削除 ");
        }
        else
        {
            Debug.LogError("削除するエフェクトがないわ！");
        }
    }
}