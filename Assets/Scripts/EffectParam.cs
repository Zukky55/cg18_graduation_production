﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable, CreateAssetMenu(fileName = "EffectParam")]
public class EffectParam : ScriptableObject
{
    public EffectType effectType;
    public int number;
    public GameObject effectObj;
    public Vector3 position;
    public Quaternion quaternion;
}
