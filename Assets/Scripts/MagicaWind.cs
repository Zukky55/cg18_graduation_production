﻿using MagicaCloth;
using System.Collections;
using UnityEngine;

public class MagicaWind : MonoBehaviour
{
    BaseCloth bc;

    [SerializeField]
    Vector3 vec;

    [SerializeField]
    float multiply = 1f;

    [SerializeField]
    PhysicsManagerTeamData.ForceMode mode;

    private void OnEnable()
    {
        bc = GetComponent<BaseCloth>();
    }

    // Update is called once per frame
    void Update()
    {

        var t = Time.timeSinceLevelLoad;

        var val = Mathf.PerlinNoise(Time.timeSinceLevelLoad, transform.position.y);
        Mathf.Sign(Mathf.Pow(Mathf.Sign(t), 2));

        bc.AddForce(vec * val * multiply, mode);
    }
}
