﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable, CreateAssetMenu(fileName = "EffectDatas")]
public class EffectDatas : ScriptableObject
{
    public List<EffectData> effectDatas;
}

[Serializable]
public class EffectData
{
    [Header("種類")]public EffectType effectType;
    [Header("データ")]public EffectParam effectParam;
    [Header("生成数")]public int generateNumber;
}
