﻿using MagicaCloth;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MagicaWindWithChildren : MonoBehaviour
{
    List<BaseCloth> baseClothes = new List<BaseCloth>();

    [SerializeField]
    Vector3 vec = Vector3.one;

    [SerializeField]
    float multiply = 1f;

    [SerializeField]
    PhysicsManagerTeamData.ForceMode mode = PhysicsManagerTeamData.ForceMode.VelocityAdd;

    private void OnEnable()
    {
        baseClothes = GetComponentsInChildren<BaseCloth>().ToList();
        if (TryGetComponent<BaseCloth>(out var bc))
        {
            baseClothes.Add(bc);
        }
    }

    private void Update()
    {
        if (!baseClothes.Any()) return;

        var val = Mathf.PerlinNoise(Time.timeSinceLevelLoad, transform.position.y);
        var v = vec * val * multiply;

        foreach (var cloth in baseClothes)
        {
            cloth.AddForce(v, mode);
        }
    }
}
