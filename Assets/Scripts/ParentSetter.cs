﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>エフェクトオブジェクトにアタッチする</summary>
public class ParentSetter : MonoBehaviour
{
    [SerializeField, Header("親オブジェクトの名前")] string parentObjectName;

    private void Start()
    {
        GameObject parentObj = GameObject.Find(parentObjectName);
        gameObject.transform.parent = parentObj.transform;
        Debug.Log($"{gameObject.transform.parent.name}を親オブジェクトに設定");
    }
}
