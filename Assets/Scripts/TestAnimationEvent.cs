﻿using System;
using UnityEngine;
using System.Collections;

namespace GraduationProduction
{
    [Serializable]
    [CreateAssetMenu(fileName = "AnimationEventParam", menuName = "AnimationEvent")]
    public class TestAnimationEvent : ScriptableObject
    {
        public GameObject effect;
        public Transform spawnNode;
    }
}
