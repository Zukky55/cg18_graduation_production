﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;
using static UnityEditor.Recorder.RecorderWindow;
using UnityEditor.Recorder.Input;
using System;

namespace UnityEditor.Recorder
{
    public class CustomRecorderWindow : EditorWindow
    {
        static readonly string s_WindowTitle = "AutoUnityRecorder";
        static readonly string s_PrefsFileName = "/../Library/Recorder/recorder.pref";
        static readonly string s_StylesFolder = "Styles/";

        public const string MenuRoot = "Window/Custom/";
        public const int MenuRootIndex = 1000;

        RecorderWindow rw;

        [MenuItem(MenuRoot + "AutoUnityRecorder")]
        public static void ShowWindow()
        {
            var instance = GetWindow<CustomRecorderWindow>(false, s_WindowTitle);
            instance.rw = GetWindow<RecorderWindow>(false, "Recorder");
        }

        RecorderItemList m_RecordingListItem;

        VisualElement m_SettingsPanel;
        VisualElement m_RecordersPanel;

        RecorderItem m_SelectedRecorderItem;

        VisualElement m_ParametersControl;
        VisualElement m_RecorderSettingPanel;

        Button m_RecordButton;
        Button m_RecordButtonIcon;

        PanelSplitter m_PanelSplitter;
        VisualElement m_AddNewRecordPanel;

        VisualElement m_RecordOptionsPanel;
        VisualElement m_RecordModeOptionsPanel;
        VisualElement m_FrameRateOptionsPanel;

        RecorderControllerSettings m_ControllerSettings;
        RecorderController m_RecorderController;

        State m_State = State.Idle;
        int m_FrameCount = 0;


        void OnEnable()
        {
            minSize = new Vector2(560.0f, 200.0f);

            var root = rootVisualElement;
            root.styleSheets.Add(Resources.Load<StyleSheet>(s_StylesFolder + "recorder"));
            root.styleSheets.Add(Resources.Load<StyleSheet>(s_StylesFolder + (EditorGUIUtility.isProSkin ? "recorder_darkSkin" : "recorder_lightSkin")));
            root.style.flexDirection = FlexDirection.Column;
            UIElementHelper.SetFocusable(root);

            var mainControls = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Row,
                    minHeight = 110.0f,
                }

            };
            root.Add(mainControls);

            var controlLeftPane = new VisualElement
            {
                style =
                {
                    minWidth = 180.0f,
                    maxWidth = 450.0f,
                    flexDirection = FlexDirection.Row,
                }
            };

            UIElementHelper.SetFlex(controlLeftPane, 0.5f);

            var controlRightPane = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column,
                }
            };

            UIElementHelper.SetFlex(controlRightPane, 0.5f);

            mainControls.Add(controlLeftPane);
            mainControls.Add(controlRightPane);

            controlLeftPane.AddToClassList("StandardPanel");
            controlRightPane.AddToClassList("StandardPanel");

            m_RecordButtonIcon = new Button(OnRecordButtonClick)
            {
                name = "recorderIcon",
                style =
                {
                    backgroundImage = Resources.Load<Texture2D>("recorder_icon"),
                }
            };

            controlLeftPane.Add(m_RecordButtonIcon);


            var leftButtonStack = new VisualElement
            {
                style =
                {
                    flexDirection = FlexDirection.Column,
                }
            };

            UIElementHelper.SetFlex(leftButtonStack, 1.0f);
        }

        void OnRecordButtonClick()
        {
            if (m_State == State.Error)
                m_State = State.Idle;

            switch (m_State)
            {
                case State.Idle:
                    {
                        StartRecording();
                        break;
                    }

                case State.WaitingForPlayModeToStartRecording:
                case State.Recording:
                    {
                        StopRecording();
                        break;
                    }
            }

            UpdateRecordButtonText();
        }

        void UpdateRecordButtonText()
        {
            m_RecordButton.text = m_State == State.Recording ? "STOP RECORDING" : "START RECORDING";
        }



        /// <summary>
        /// Used to Start the recording with current settings.
        /// If not already the case, the Editor will also switch to PlayMode.
        /// </summary>
        public void StartRecording()
        {
            //if (m_State == State.Idle)
            //{
            //    m_State = State.WaitingForPlayModeToStartRecording;
            //    GameViewSize.DisableMaxOnPlay();
            //    EditorApplication.isPlaying = true;
            //    m_FrameCount = Time.frameCount;
            Debug.Log($"Push StartRecording");
        }

        /// <summary>
        /// Used to Stop current recordings if any.
        /// Exiting PlayMode while the Recorder is recording will automatically Stop the recorder.
        /// </summary>
        public void StopRecording()
        {
            //if (IsRecording())
            //    StopRecordingInternal();
            Debug.Log($"Push StopRecording");
        }
    }
}