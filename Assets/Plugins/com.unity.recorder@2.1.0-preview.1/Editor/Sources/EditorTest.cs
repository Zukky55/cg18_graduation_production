﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class EditorTest : ScriptableObject
{
    [MenuItem("Tools/SceneChange")]
    static void SceneChange()
    {
        EditorSceneManager.OpenScene("Scenes/MagicaClothTest");
    }
}