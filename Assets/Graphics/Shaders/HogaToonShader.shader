// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VGA/Hogawish/HoagToonShader"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_MainTex("MainTex", 2D) = "white" {}
		_ShadowTex("ShadowTex", 2D) = "white" {}
		_SpeularTex("SpeularTex", 2D) = "black" {}
		_ForceShadowTex("ForceShadowTex", 2D) = "white" {}
		_shadowThreshold("shadowThreshold", Range( 0 , 1)) = 0.5
		_shadowJitter("shadowJitter", Range( 0 , 0.5)) = 0.1
		_ToonPower("ToonPower", Range( 0 , 1)) = 1
		_MatteColor("MatteColor", Color) = (0,1,0,0)
		[Toggle(_CHANGEMATTE_ON)] _ChangeMatte("ChangeMatte", Float) = 0
		[Toggle(_HIDE_ON)] _Hide("Hide", Float) = 0
		_RoomColorStrength("RoomColorStrength", Range( 0 , 1)) = 0
		_RoomColor("RoomColor", Color) = (0,0,0,0)
		_RoomColorPower("RoomColorPower", Float) = 0.2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityCG.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _CHANGEMATTE_ON
		#pragma shader_feature_local _HIDE_ON
		struct Input
		{
			float3 worldPos;
			float3 worldNormal;
			float2 uv_texcoord;
		};

		uniform float4 _RoomColor;
		uniform float _RoomColorStrength;
		uniform float _RoomColorPower;
		uniform float _shadowThreshold;
		uniform float _shadowJitter;
		uniform sampler2D _ShadowTex;
		uniform float4 _ShadowTex_ST;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform sampler2D _ForceShadowTex;
		uniform float4 _ForceShadowTex_ST;
		uniform float _ToonPower;
		uniform float4 _MatteColor;
		uniform sampler2D _SpeularTex;
		uniform float4 _SpeularTex_ST;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 _Vector0 = float3(0,0,0);
			o.Albedo = _Vector0;
			float4 color46 = IsGammaSpace() ? float4(1,1,1,0) : float4(1,1,1,0);
			float4 lerpResult47 = lerp( color46 , _RoomColor , ( _RoomColorStrength * _RoomColorPower ));
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldNormal = i.worldNormal;
			float dotResult3 = dot( ase_worldlightDir , ase_worldNormal );
			float temp_output_17_0 = saturate( ( ( saturate( dotResult3 ) - ( _shadowThreshold - ( _shadowJitter / 2.0 ) ) ) / _shadowJitter ) );
			float2 uv_ShadowTex = i.uv_texcoord * _ShadowTex_ST.xy + _ShadowTex_ST.zw;
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float2 uv_ForceShadowTex = i.uv_texcoord * _ForceShadowTex_ST.xy + _ForceShadowTex_ST.zw;
			float4 lerpResult5 = lerp( ( ( 1.0 - ( lerpResult47 * temp_output_17_0 ) ) * float4( (tex2D( _ShadowTex, uv_ShadowTex )).rgb , 0.0 ) ) , ( lerpResult47 * float4( (tex2D( _MainTex, uv_MainTex )).rgb , 0.0 ) ) , ( temp_output_17_0 * tex2D( _ForceShadowTex, uv_ForceShadowTex ).r ));
			#ifdef _CHANGEMATTE_ON
				float4 staticSwitch36 = float4( (_MatteColor).rgb , 0.0 );
			#else
				float4 staticSwitch36 = ( lerpResult5 * _ToonPower );
			#endif
			o.Emission = staticSwitch36.rgb;
			float2 uv_SpeularTex = i.uv_texcoord * _SpeularTex_ST.xy + _SpeularTex_ST.zw;
			#ifdef _CHANGEMATTE_ON
				float3 staticSwitch37 = _Vector0;
			#else
				float3 staticSwitch37 = (tex2D( _SpeularTex, uv_SpeularTex )).rgb;
			#endif
			o.Specular = staticSwitch37;
			o.Alpha = 1;
			float3 temp_cast_4 = (1.0).xxx;
			#ifdef _HIDE_ON
				float3 staticSwitch38 = _Vector0;
			#else
				float3 staticSwitch38 = temp_cast_4;
			#endif
			clip( staticSwitch38.x - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17600
446;166;1416;783;1294.599;800.3559;2.539455;True;True
Node;AmplifyShaderEditor.WorldNormalVector;1;-2266.529,-18.76757;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;2;-2253.573,-192.1083;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;14;-2323.428,408.5299;Inherit;False;Property;_shadowJitter;shadowJitter;6;0;Create;True;0;0;False;0;0.1;0.015;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;21;-1756.973,230.4798;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;3;-1995.868,-71.28671;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-2194.713,130.6424;Inherit;False;Property;_shadowThreshold;shadowThreshold;5;0;Create;True;0;0;False;0;0.5;0.165;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;11;-1794.819,-44.99129;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;22;-1633.239,126.917;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-1842.219,-697.6617;Inherit;False;Property;_RoomColorStrength;RoomColorStrength;11;0;Create;True;0;0;False;0;0;0.33;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;50;-1646.616,-551.6057;Inherit;False;Property;_RoomColorPower;RoomColorPower;13;0;Create;True;0;0;False;0;0.2;0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;15;-1468.518,10.46165;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;23;-1434.492,335.8079;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;46;-1763.574,-1027.377;Inherit;False;Constant;_Color0;Color 0;14;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;45;-1893.407,-884.6478;Inherit;False;Property;_RoomColor;RoomColor;12;0;Create;True;0;0;False;0;0,0,0,0;0,1,0.8651767,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;16;-1164.142,34.66274;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;49;-1436.241,-605.0585;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;17;-1022.307,-149.5389;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;47;-1076.554,-694.6558;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WireNode;55;-783.3147,-569.2371;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;6;-1183.781,-1303.853;Inherit;True;Property;_ShadowTex;ShadowTex;2;0;Create;True;0;0;False;0;None;8c1bdbae34e89c943b4ff7be77f95b21;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.WireNode;54;-741.838,-178.1244;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;4;-1805.886,-406.6258;Inherit;True;Property;_MainTex;MainTex;1;0;Create;True;0;0;False;0;None;0d80c67282211954f8b88e42d6f92aa4;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;8;-795.0804,-1284.353;Inherit;True;Property;_TextureSample1;Texture Sample 1;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;56;-712.3679,-829.2066;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;7;-1414.586,-359.8263;Inherit;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;24;-1786.822,485.9118;Inherit;True;Property;_ForceShadowTex;ForceShadowTex;4;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.ComponentMaskNode;10;-449.7025,-1284.06;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;57;-486.1414,-782.088;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;9;-1071.809,-362.0358;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;25;-1470.39,491.0343;Inherit;True;Property;_TextureSample2;Texture Sample 2;6;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;-248.4693,-778.192;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-597.1808,-446.0652;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-665.1005,181.5351;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;27;-879.513,827.1264;Inherit;True;Property;_SpeularTex;SpeularTex;3;0;Create;True;0;0;False;0;None;None;False;black;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;28;-388.573,584.2191;Inherit;True;Property;_TextureSample3;Texture Sample 3;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-405.3505,160.1951;Inherit;False;Property;_ToonPower;ToonPower;7;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;5;-305.6144,-302.6692;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;31;69.78761,-835.6061;Inherit;False;Property;_MatteColor;MatteColor;8;0;Create;True;0;0;False;0;0,1,0,0;0,1,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;41;542.1719,908.9948;Inherit;False;Constant;_Float0;Float 0;12;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;32;108.581,-520.8604;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;153.2669,-229.7602;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector3Node;18;285.4489,630.3127;Inherit;False;Constant;_Vector0;Vector 0;4;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ComponentMaskNode;34;-85.20807,463.6518;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;37;143.9548,245.7427;Inherit;False;Property;_ChangeMatte;ChangeMatte;9;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;38;580.4047,473.4523;Inherit;False;Property;_Hide;Hide;10;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;36;549.9445,-320.328;Inherit;False;Property;_ChangeMatte;ChangeMatte;9;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1342.906,-664.1371;Float;False;True;-1;2;ASEMaterialInspector;0;0;StandardSpecular;VGA/Hogawish/HoagToonShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;TransparentCutout;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;21;0;14;0
WireConnection;3;0;2;0
WireConnection;3;1;1;0
WireConnection;11;0;3;0
WireConnection;22;0;13;0
WireConnection;22;1;21;0
WireConnection;15;0;11;0
WireConnection;15;1;22;0
WireConnection;23;0;14;0
WireConnection;16;0;15;0
WireConnection;16;1;23;0
WireConnection;49;0;44;0
WireConnection;49;1;50;0
WireConnection;17;0;16;0
WireConnection;47;0;46;0
WireConnection;47;1;45;0
WireConnection;47;2;49;0
WireConnection;55;0;47;0
WireConnection;54;0;17;0
WireConnection;8;0;6;0
WireConnection;56;0;55;0
WireConnection;56;1;54;0
WireConnection;7;0;4;0
WireConnection;10;0;8;0
WireConnection;57;0;56;0
WireConnection;9;0;7;0
WireConnection;25;0;24;0
WireConnection;58;0;57;0
WireConnection;58;1;10;0
WireConnection;48;0;55;0
WireConnection;48;1;9;0
WireConnection;26;0;17;0
WireConnection;26;1;25;1
WireConnection;28;0;27;0
WireConnection;5;0;58;0
WireConnection;5;1;48;0
WireConnection;5;2;26;0
WireConnection;32;0;31;0
WireConnection;20;0;5;0
WireConnection;20;1;19;0
WireConnection;34;0;28;0
WireConnection;37;1;34;0
WireConnection;37;0;18;0
WireConnection;38;1;41;0
WireConnection;38;0;18;0
WireConnection;36;1;20;0
WireConnection;36;0;32;0
WireConnection;0;0;18;0
WireConnection;0;2;36;0
WireConnection;0;3;37;0
WireConnection;0;10;38;0
ASEEND*/
//CHKSM=50C32280368E3EB69BD01B3A86BDE4B3CB0A3E61