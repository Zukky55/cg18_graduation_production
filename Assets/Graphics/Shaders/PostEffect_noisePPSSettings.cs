// Amplify Shader Editor - Visual Shader Editing Tool
// Copyright (c) Amplify Creations, Lda <info@amplify.pt>
#if UNITY_POST_PROCESSING_STACK_V2
using System;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

[Serializable]
[PostProcess( typeof( PostEffect_noisePPSRenderer ), PostProcessEvent.AfterStack, "PostEffect_noise", true )]
public sealed class PostEffect_noisePPSSettings : PostProcessEffectSettings
{
	[Tooltip( "Screen" )]
	public TextureParameter _MainTex = new TextureParameter {  };
	[Tooltip( "noiseBrend" )]
	public FloatParameter _noiseBrend = new FloatParameter { value = 0.5f };
}

public sealed class PostEffect_noisePPSRenderer : PostProcessEffectRenderer<PostEffect_noisePPSSettings>
{
	public override void Render( PostProcessRenderContext context )
	{
		var sheet = context.propertySheets.Get( Shader.Find( "PostEffect_noise" ) );
		if(settings._MainTex.value != null) sheet.properties.SetTexture( "_MainTex", settings._MainTex );
		sheet.properties.SetFloat( "_noiseBrend", settings._noiseBrend );
		context.command.BlitFullscreenTriangle( context.source, context.destination, sheet, 0 );
	}
}
#endif
