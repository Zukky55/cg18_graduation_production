// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VGA/Hogawish/WingShader"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_Txture1("Txture1", 2D) = "white" {}
		_OutLinColor("OutLinColor", 2D) = "white" {}
		_Txture3("Txture3", 2D) = "white" {}
		_Txture2("Txture2", 2D) = "white" {}
		_ToonPower("ToonPower", Range( 0 , 1)) = 1
		_MatteColor("MatteColor", Color) = (0,1,0,0)
		_Color0("Color 0", Color) = (0,1,0,0)
		[Toggle(_HIDE_ON)] _Hide("Hide", Float) = 0
		[Toggle(_CHANGEMATTE_ON)] _ChangeMatte("ChangeMatte", Float) = 0
		_OutlineAmplify("OutlineAmplify", Float) = 0
		_OutlineFrequencyX("OutlineFrequencyX", Float) = 0
		_OutlineFrequencyT("OutlineFrequencyT", Float) = 0
		_OutLineColorSpeed("OutLineColorSpeed", Float) = 0
		_speed1("speed1", Float) = 0
		_speed2("speed2", Float) = 0
		_speed3("speed3", Float) = 0
		_Texture1Grids("Texture1Grids", Vector) = (5,5,0,0)
		_Vector2("Vector 2", Vector) = (5,5,0,0)
		_Texture2Grids("Texture2Grids", Vector) = (5,5,0,0)
		_Vector1("Vector 1", Vector) = (5,5,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ }
		Cull Front
		CGPROGRAM
		#pragma target 3.0
		#pragma surface outlineSurf Outline nofog  keepalpha noshadow noambient novertexlights nolightmap nodynlightmap nodirlightmap nometa noforwardadd vertex:outlineVertexDataFunc 
		void outlineVertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_vertex3Pos = v.vertex.xyz;
			float4 appendResult71 = (float4(0.0 , ( sin( ( ( _Time.y * _OutlineFrequencyT ) + ( ase_vertex3Pos.x * _OutlineFrequencyX ) ) ) * _OutlineAmplify ) , 0.0 , 0.0));
			float3 outlineVar = appendResult71.xyz;
			v.vertex.xyz += outlineVar;
		}
		inline half4 LightingOutline( SurfaceOutput s, half3 lightDir, half atten ) { return half4 ( 0,0,0, s.Alpha); }
		void outlineSurf( Input i, inout SurfaceOutput o )
		{
			// *** BEGIN Flipbook UV Animation vars ***
			// Total tiles of Flipbook Texture
			float fbtotaltiles128 = _Vector2.x * _Vector2.y;
			// Offsets for cols and rows of Flipbook Texture
			float fbcolsoffset128 = 1.0f / _Vector2.x;
			float fbrowsoffset128 = 1.0f / _Vector2.y;
			// Speed of animation
			float fbspeed128 = _Time[ 1 ] * _OutLineColorSpeed;
			// UV Tiling (col and row offset)
			float2 fbtiling128 = float2(fbcolsoffset128, fbrowsoffset128);
			// UV Offset - calculate current tile linear index, and convert it to (X * coloffset, Y * rowoffset)
			// Calculate current tile linear index
			float fbcurrenttileindex128 = round( fmod( fbspeed128 + 0.0, fbtotaltiles128) );
			fbcurrenttileindex128 += ( fbcurrenttileindex128 < 0) ? fbtotaltiles128 : 0;
			// Obtain Offset X coordinate from current tile linear index
			float fblinearindextox128 = round ( fmod ( fbcurrenttileindex128, _Vector2.x ) );
			// Multiply Offset X by coloffset
			float fboffsetx128 = fblinearindextox128 * fbcolsoffset128;
			// Obtain Offset Y coordinate from current tile linear index
			float fblinearindextoy128 = round( fmod( ( fbcurrenttileindex128 - fblinearindextox128 ) / _Vector2.x, _Vector2.y ) );
			// Reverse Y to get tiles from Top to Bottom
			fblinearindextoy128 = (int)(_Vector2.y-1) - fblinearindextoy128;
			// Multiply Offset Y by rowoffset
			float fboffsety128 = fblinearindextoy128 * fbrowsoffset128;
			// UV Offset
			float2 fboffset128 = float2(fboffsetx128, fboffsety128);
			// Flipbook UV
			half2 fbuv128 = i.uv_texcoord * fbtiling128 + fboffset128;
			// *** END Flipbook UV Animation vars ***
			#ifdef _CHANGEMATTE_ON
				float4 staticSwitch107 = float4( (_Color0).rgb , 0.0 );
			#else
				float4 staticSwitch107 = tex2D( _OutLinColor, fbuv128 );
			#endif
			o.Emission = staticSwitch107.rgb;
		}
		ENDCG
		

		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _CHANGEMATTE_ON
		#pragma shader_feature_local _HIDE_ON
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Txture3;
		uniform float2 _Vector1;
		uniform float _speed3;
		uniform sampler2D _Txture2;
		uniform float2 _Texture2Grids;
		uniform float _speed2;
		uniform sampler2D _Txture1;
		uniform float2 _Texture1Grids;
		uniform float _speed1;
		uniform float _ToonPower;
		uniform float4 _MatteColor;
		uniform float _Cutoff = 0.5;
		uniform float _OutlineFrequencyT;
		uniform float _OutlineFrequencyX;
		uniform float _OutlineAmplify;
		uniform sampler2D _OutLinColor;
		uniform float2 _Vector2;
		uniform float _OutLineColorSpeed;
		uniform float4 _Color0;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			v.vertex.xyz += 0;
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 _Vector0 = float3(0,0,0);
			o.Albedo = _Vector0;
			// *** BEGIN Flipbook UV Animation vars ***
			// Total tiles of Flipbook Texture
			float fbtotaltiles123 = _Vector1.x * _Vector1.y;
			// Offsets for cols and rows of Flipbook Texture
			float fbcolsoffset123 = 1.0f / _Vector1.x;
			float fbrowsoffset123 = 1.0f / _Vector1.y;
			// Speed of animation
			float fbspeed123 = _Time[ 1 ] * _speed3;
			// UV Tiling (col and row offset)
			float2 fbtiling123 = float2(fbcolsoffset123, fbrowsoffset123);
			// UV Offset - calculate current tile linear index, and convert it to (X * coloffset, Y * rowoffset)
			// Calculate current tile linear index
			float fbcurrenttileindex123 = round( fmod( fbspeed123 + 0.0, fbtotaltiles123) );
			fbcurrenttileindex123 += ( fbcurrenttileindex123 < 0) ? fbtotaltiles123 : 0;
			// Obtain Offset X coordinate from current tile linear index
			float fblinearindextox123 = round ( fmod ( fbcurrenttileindex123, _Vector1.x ) );
			// Multiply Offset X by coloffset
			float fboffsetx123 = fblinearindextox123 * fbcolsoffset123;
			// Obtain Offset Y coordinate from current tile linear index
			float fblinearindextoy123 = round( fmod( ( fbcurrenttileindex123 - fblinearindextox123 ) / _Vector1.x, _Vector1.y ) );
			// Reverse Y to get tiles from Top to Bottom
			fblinearindextoy123 = (int)(_Vector1.y-1) - fblinearindextoy123;
			// Multiply Offset Y by rowoffset
			float fboffsety123 = fblinearindextoy123 * fbrowsoffset123;
			// UV Offset
			float2 fboffset123 = float2(fboffsetx123, fboffsety123);
			// Flipbook UV
			half2 fbuv123 = i.uv_texcoord * fbtiling123 + fboffset123;
			// *** END Flipbook UV Animation vars ***
			float fbtotaltiles105 = _Texture2Grids.x * _Texture2Grids.y;
			float fbcolsoffset105 = 1.0f / _Texture2Grids.x;
			float fbrowsoffset105 = 1.0f / _Texture2Grids.y;
			float fbspeed105 = _Time[ 1 ] * _speed2;
			float2 fbtiling105 = float2(fbcolsoffset105, fbrowsoffset105);
			float fbcurrenttileindex105 = round( fmod( fbspeed105 + 0.0, fbtotaltiles105) );
			fbcurrenttileindex105 += ( fbcurrenttileindex105 < 0) ? fbtotaltiles105 : 0;
			float fblinearindextox105 = round ( fmod ( fbcurrenttileindex105, _Texture2Grids.x ) );
			float fboffsetx105 = fblinearindextox105 * fbcolsoffset105;
			float fblinearindextoy105 = round( fmod( ( fbcurrenttileindex105 - fblinearindextox105 ) / _Texture2Grids.x, _Texture2Grids.y ) );
			fblinearindextoy105 = (int)(_Texture2Grids.y-1) - fblinearindextoy105;
			float fboffsety105 = fblinearindextoy105 * fbrowsoffset105;
			float2 fboffset105 = float2(fboffsetx105, fboffsety105);
			half2 fbuv105 = i.uv_texcoord * fbtiling105 + fboffset105;
			float fbtotaltiles94 = _Texture1Grids.x * _Texture1Grids.y;
			float fbcolsoffset94 = 1.0f / _Texture1Grids.x;
			float fbrowsoffset94 = 1.0f / _Texture1Grids.y;
			float fbspeed94 = _Time[ 1 ] * _speed1;
			float2 fbtiling94 = float2(fbcolsoffset94, fbrowsoffset94);
			float fbcurrenttileindex94 = round( fmod( fbspeed94 + 0.0, fbtotaltiles94) );
			fbcurrenttileindex94 += ( fbcurrenttileindex94 < 0) ? fbtotaltiles94 : 0;
			float fblinearindextox94 = round ( fmod ( fbcurrenttileindex94, _Texture1Grids.x ) );
			float fboffsetx94 = fblinearindextox94 * fbcolsoffset94;
			float fblinearindextoy94 = round( fmod( ( fbcurrenttileindex94 - fblinearindextox94 ) / _Texture1Grids.x, _Texture1Grids.y ) );
			fblinearindextoy94 = (int)(_Texture1Grids.y-1) - fblinearindextoy94;
			float fboffsety94 = fblinearindextoy94 * fbrowsoffset94;
			float2 fboffset94 = float2(fboffsetx94, fboffsety94);
			half2 fbuv94 = i.uv_texcoord * fbtiling94 + fboffset94;
			float4 tex2DNode7 = tex2D( _Txture1, fbuv94 );
			#ifdef _CHANGEMATTE_ON
				float4 staticSwitch36 = float4( (_MatteColor).rgb , 0.0 );
			#else
				float4 staticSwitch36 = ( saturate( ( tex2D( _Txture3, fbuv123 ) + ( tex2D( _Txture2, fbuv105 ).r * tex2DNode7 ) ) ) * _ToonPower );
			#endif
			o.Emission = staticSwitch36.rgb;
			#ifdef _CHANGEMATTE_ON
				float3 staticSwitch37 = _Vector0;
			#else
				float3 staticSwitch37 = float3( 0,0,0 );
			#endif
			o.Specular = staticSwitch37;
			o.Alpha = tex2DNode7.r;
			float3 temp_cast_3 = (1.0).xxx;
			#ifdef _HIDE_ON
				float3 staticSwitch38 = _Vector0;
			#else
				float3 staticSwitch38 = temp_cast_3;
			#endif
			clip( staticSwitch38.x - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17600
1922;562;1180;805;4459.59;1822.914;5.922964;True;True
Node;AmplifyShaderEditor.CommentaryNode;84;75.12097,1062.52;Inherit;False;2652.4;1593.475;Outline;21;117;107;64;71;108;88;82;106;85;92;93;78;126;127;128;125;80;83;87;66;86;;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;103;-1816.956,-1382.737;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;95;-2494.647,-119.0666;Inherit;False;Property;_Texture1Grids;Texture1Grids;17;0;Create;True;0;0;False;0;5,5;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;99;-2471.447,-451.4651;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;104;-1783.861,-1107.305;Inherit;False;Property;_Texture2Grids;Texture2Grids;19;0;Create;True;0;0;False;0;5,5;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;101;-1673.43,-974.5617;Inherit;False;Property;_speed2;speed2;15;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;96;-2438.127,13.67676;Inherit;False;Property;_speed1;speed1;14;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;105;-1611.643,-978.8837;Inherit;True;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;100;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;122;-2585.23,401.2249;Inherit;False;Property;_Vector1;Vector 1;20;0;Create;True;0;0;False;0;5,5;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;94;-2100.312,-176.2043;Inherit;True;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;100;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;121;-2847.103,696.947;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;120;-2990.622,904.1904;Inherit;False;Property;_speed3;speed3;16;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;80;1259.556,2314.139;Inherit;False;Property;_OutlineFrequencyX;OutlineFrequencyX;11;0;Create;True;0;0;False;0;0;18.07;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;83;1114.178,2115.744;Inherit;True;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;78;1282.301,1923.997;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;102;-1398.072,-1202.28;Inherit;True;Property;_Txture2;Txture2;4;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;93;1279.415,2023.634;Inherit;False;Property;_OutlineFrequencyT;OutlineFrequencyT;12;0;Create;True;0;0;False;0;0;15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;4;-1980.928,-473.0218;Inherit;True;Property;_Txture1;Txture1;1;0;Create;True;0;0;False;0;None;36f1918dee0a3404e97f8c493634423e;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;123;-2280.424,778.6664;Inherit;True;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;100;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;92;1544.51,1951.086;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;124;-1922.942,130.3329;Inherit;True;Property;_Txture3;Txture3;3;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;100;-915.9321,-753.7271;Inherit;True;Property;_TextureSample4;Texture Sample 4;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;7;-1293.004,-360.5368;Inherit;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;87;1543.406,2136.46;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;127;559.6481,1440.821;Inherit;False;Property;_Vector2;Vector 2;18;0;Create;True;0;0;False;0;5,5;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;126;582.8481,1108.422;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;114;-429.3518,-416.9676;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;85;1770.156,1987.874;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;125;539.1298,1843.406;Inherit;False;Property;_OutLineColorSpeed;OutLineColorSpeed;13;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;118;-1115.019,216.7532;Inherit;True;Property;_TextureSample1;Texture Sample 1;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;106;1348.047,1809.697;Inherit;False;Property;_Color0;Color 0;7;0;Create;True;0;0;False;0;0,1,0,0;1,0.03301889,0.03301889,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SinOpNode;82;2006.069,2035.754;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;1917.201,2262.202;Inherit;False;Property;_OutlineAmplify;OutlineAmplify;10;0;Create;True;0;0;False;0;0;0.04;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;130;-382.5615,-190.0266;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;117;1219.726,1502.085;Inherit;True;Property;_OutLinColor;OutLinColor;2;0;Create;True;0;0;False;0;None;36f1918dee0a3404e97f8c493634423e;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;128;945.5838,1656.403;Inherit;True;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;100;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.ComponentMaskNode;108;1735.439,1826.941;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;86;2187.566,2086.707;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;88;1480.751,1596.878;Inherit;True;Property;_OutlineColor;OutlineColor;10;0;Create;True;0;0;False;0;-1;None;630c6027569e3ba48b42db2f22c92839;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;31;-208.5766,-649.588;Inherit;False;Property;_MatteColor;MatteColor;6;0;Create;True;0;0;False;0;0,1,0,0;0,1,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-198.3505,150.1951;Inherit;False;Property;_ToonPower;ToonPower;5;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;131;-89.51822,-267.0012;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;107;1995.706,1654.179;Inherit;False;Property;_ChangeMatte;ChangeMatte;9;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;71;2436.75,1930.611;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;41;448.0484,-242.8336;Inherit;False;Constant;_Float0;Float 0;12;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;18;478.7098,10.7601;Inherit;False;Constant;_Vector0;Vector 0;4;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;153.2669,-229.7602;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ComponentMaskNode;32;82.54137,-736.2791;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OutlineNode;64;2366.227,1661.411;Inherit;False;2;True;None;0;0;Front;3;0;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WireNode;113;-750.3034,-1059.398;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;38;820.2441,-302.2214;Inherit;False;Property;_Hide;Hide;8;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;37;692.3592,-718.9949;Inherit;False;Property;_ChangeMatte;ChangeMatte;8;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;36;653.0848,-1003.391;Inherit;False;Property;_ChangeMatte;ChangeMatte;9;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1441.441,-1460.038;Float;False;True;-1;2;ASEMaterialInspector;0;0;StandardSpecular;VGA/Hogawish/WingShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Transparent;;Geometry;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;105;0;103;0
WireConnection;105;1;104;1
WireConnection;105;2;104;2
WireConnection;105;3;101;0
WireConnection;94;0;99;0
WireConnection;94;1;95;1
WireConnection;94;2;95;2
WireConnection;94;3;96;0
WireConnection;123;0;121;0
WireConnection;123;1;122;1
WireConnection;123;2;122;2
WireConnection;123;3;120;0
WireConnection;92;0;78;0
WireConnection;92;1;93;0
WireConnection;100;0;102;0
WireConnection;100;1;105;0
WireConnection;7;0;4;0
WireConnection;7;1;94;0
WireConnection;87;0;83;1
WireConnection;87;1;80;0
WireConnection;114;0;100;1
WireConnection;114;1;7;0
WireConnection;85;0;92;0
WireConnection;85;1;87;0
WireConnection;118;0;124;0
WireConnection;118;1;123;0
WireConnection;82;0;85;0
WireConnection;130;0;118;0
WireConnection;130;1;114;0
WireConnection;128;0;126;0
WireConnection;128;1;127;1
WireConnection;128;2;127;2
WireConnection;128;3;125;0
WireConnection;108;0;106;0
WireConnection;86;0;82;0
WireConnection;86;1;66;0
WireConnection;88;0;117;0
WireConnection;88;1;128;0
WireConnection;131;0;130;0
WireConnection;107;1;88;0
WireConnection;107;0;108;0
WireConnection;71;1;86;0
WireConnection;20;0;131;0
WireConnection;20;1;19;0
WireConnection;32;0;31;0
WireConnection;64;0;107;0
WireConnection;64;1;71;0
WireConnection;113;0;7;0
WireConnection;38;1;41;0
WireConnection;38;0;18;0
WireConnection;37;0;18;0
WireConnection;36;1;20;0
WireConnection;36;0;32;0
WireConnection;0;0;18;0
WireConnection;0;2;36;0
WireConnection;0;3;37;0
WireConnection;0;9;113;0
WireConnection;0;10;38;0
WireConnection;0;11;64;0
ASEEND*/
//CHKSM=5B48D485DA2009AA26259AB90421A9F1C202B32D