// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VGA/Hogawish/EyeShader"
{
	Properties
	{
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_MainTex("MainTex", 2D) = "white" {}
		_HighlightTex("HighlightTex", 2D) = "white" {}
		_ShadowTex("ShadowTex", 2D) = "white" {}
		_SpeularTex("SpeularTex", 2D) = "black" {}
		_ForceShadowTex("ForceShadowTex", 2D) = "white" {}
		_shadowThreshold("shadowThreshold", Range( 0 , 1)) = 0.5
		_shadowJitter("shadowJitter", Range( 0 , 0.5)) = 0.1
		_ToonPower("ToonPower", Range( 0 , 1)) = 1
		_MatteColor("MatteColor", Color) = (0,1,0,0)
		[Toggle(_HIDE_ON)] _Hide("Hide", Float) = 0
		_RoomColorStrength("RoomColorStrength", Range( 0 , 1)) = 0
		[Toggle(_CHANGEMATTE_ON)] _ChangeMatte("ChangeMatte", Float) = 0
		_RoomColor("RoomColor", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGINCLUDE
		#include "UnityCG.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _CHANGEMATTE_ON
		#pragma shader_feature_local _HIDE_ON
		struct Input
		{
			float2 uv_texcoord;
			float3 worldPos;
			float3 worldNormal;
		};

		uniform float4 _RoomColor;
		uniform float _RoomColorStrength;
		uniform sampler2D _ShadowTex;
		uniform float4 _ShadowTex_ST;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;
		uniform float _shadowThreshold;
		uniform float _shadowJitter;
		uniform sampler2D _ForceShadowTex;
		uniform float4 _ForceShadowTex_ST;
		uniform sampler2D _HighlightTex;
		uniform float4 _HighlightTex_ST;
		uniform float _ToonPower;
		uniform float4 _MatteColor;
		uniform sampler2D _SpeularTex;
		uniform float4 _SpeularTex_ST;
		uniform float _Cutoff = 0.5;

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float3 _Vector0 = float3(0,0,0);
			o.Albedo = _Vector0;
			float4 color70 = IsGammaSpace() ? float4(1,1,1,0) : float4(1,1,1,0);
			float4 lerpResult69 = lerp( color70 , _RoomColor , ( _RoomColorStrength * 0.2 ));
			float2 uv_ShadowTex = i.uv_texcoord * _ShadowTex_ST.xy + _ShadowTex_ST.zw;
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float3 ase_worldPos = i.worldPos;
			#if defined(LIGHTMAP_ON) && UNITY_VERSION < 560 //aseld
			float3 ase_worldlightDir = 0;
			#else //aseld
			float3 ase_worldlightDir = normalize( UnityWorldSpaceLightDir( ase_worldPos ) );
			#endif //aseld
			float3 ase_worldNormal = i.worldNormal;
			float dotResult33 = dot( ase_worldlightDir , ase_worldNormal );
			float temp_output_41_0 = saturate( ( ( saturate( dotResult33 ) - ( _shadowThreshold - ( _shadowJitter / 2.0 ) ) ) / _shadowJitter ) );
			float2 uv_ForceShadowTex = i.uv_texcoord * _ForceShadowTex_ST.xy + _ForceShadowTex_ST.zw;
			float3 lerpResult43 = lerp( (tex2D( _ShadowTex, uv_ShadowTex )).rgb , (tex2D( _MainTex, uv_MainTex )).rgb , ( temp_output_41_0 * tex2D( _ForceShadowTex, uv_ForceShadowTex ).r ));
			float2 uv_HighlightTex = i.uv_texcoord * _HighlightTex_ST.xy + _HighlightTex_ST.zw;
			#ifdef _CHANGEMATTE_ON
				float4 staticSwitch68 = float4( (_MatteColor).rgb , 0.0 );
			#else
				float4 staticSwitch68 = ( ( lerpResult69 * saturate( ( float4( lerpResult43 , 0.0 ) + ( tex2D( _HighlightTex, uv_HighlightTex ) * max( 0.3 , temp_output_41_0 ) ) ) ) ) * _ToonPower );
			#endif
			o.Emission = staticSwitch68.rgb;
			float2 uv_SpeularTex = i.uv_texcoord * _SpeularTex_ST.xy + _SpeularTex_ST.zw;
			#ifdef _CHANGEMATTE_ON
				float3 staticSwitch65 = _Vector0;
			#else
				float3 staticSwitch65 = (tex2D( _SpeularTex, uv_SpeularTex )).rgb;
			#endif
			o.Specular = staticSwitch65;
			o.Alpha = 1;
			float3 temp_cast_3 = (1.0).xxx;
			#ifdef _HIDE_ON
				float3 staticSwitch64 = _Vector0;
			#else
				float3 staticSwitch64 = temp_cast_3;
			#endif
			clip( staticSwitch64.x - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf StandardSpecular keepalpha fullforwardshadows 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float3 worldNormal : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.worldNormal = worldNormal;
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = IN.worldNormal;
				SurfaceOutputStandardSpecular o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandardSpecular, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17600
-36.8;156;1524;791;2622.689;973.4578;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;28;-2490.877,600.1412;Inherit;False;Property;_shadowJitter;shadowJitter;7;0;Create;True;0;0;False;0;0.1;0.1;0;0.5;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;29;-2502.494,292.0887;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;30;-2490.911,144.8678;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DotProductOpNode;33;-2125.652,192.5813;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;31;-2042.356,492.7691;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;32;-2479.97,442.6169;Inherit;False;Property;_shadowThreshold;shadowThreshold;6;0;Create;True;0;0;False;0;0.5;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;34;-1924.842,247.6566;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;35;-1898.056,347.169;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;36;-1647.171,551.7354;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;37;-1736.467,247.5566;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;51;-2550.673,-166.1303;Inherit;True;Property;_MainTex;MainTex;1;0;Create;True;0;0;False;0;None;d97d1643ad6aa824a81e65d5ad40a739;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;38;-1577.285,216.9477;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;39;-1987.121,681.2011;Inherit;True;Property;_ForceShadowTex;ForceShadowTex;5;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.TexturePropertyNode;52;-2526.753,-372.9056;Inherit;True;Property;_ShadowTex;ShadowTex;3;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;50;-2026.653,-191.4881;Inherit;True;Property;_TextureSample1;Texture Sample 1;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;53;-2204.559,-402.1994;Inherit;True;Property;_TextureSample2;Texture Sample 2;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;56;-2227.99,-776.3774;Inherit;True;Property;_HighlightTex;HighlightTex;2;0;Create;True;0;0;False;0;None;1efce263a1e6227448497828f1f9ab09;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SaturateNode;41;-1470.427,14.04328;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;80;-1431.452,-397.128;Inherit;False;Constant;_Float2;Float 2;15;0;Create;True;0;0;False;0;0.3;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;40;-1589.751,553.5828;Inherit;True;Property;_TextureSample3;Texture Sample 3;6;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMaxOpNode;79;-1258.514,-404.4559;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;54;-1675.429,-330.0955;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;55;-1692.339,-729.1415;Inherit;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ComponentMaskNode;49;-1623.425,-140.4234;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-1135.762,353.4668;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;75;-911.6761,-383.7569;Inherit;False;Constant;_Float0;Float 0;15;0;Create;True;0;0;False;0;0.2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;77;-1091.218,-390.6415;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;72;-1051.469,-480.7729;Inherit;False;Property;_RoomColorStrength;RoomColorStrength;11;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;43;-1205.448,-132.7666;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;70;-1055.667,-1063.132;Inherit;False;Constant;_Color0;Color 0;14;0;Create;True;0;0;False;0;1,1,1,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;71;-1062.507,-862.1088;Inherit;False;Property;_RoomColor;RoomColor;13;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;74;-737.4296,-455.1257;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;57;-845.9683,-180.8244;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;59;-1166.576,1263.896;Inherit;True;Property;_SpeularTex;SpeularTex;4;0;Create;True;0;0;False;0;None;None;False;black;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.LerpOp;69;-553.5348,-548.5912;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;58;-716.2665,-131.9286;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;73;-473.0396,-80.32127;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-809.567,467.2541;Inherit;False;Property;_ToonPower;ToonPower;8;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;60;-675.6364,1020.989;Inherit;True;Property;_TextureSample4;Texture Sample 4;3;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;66;-546.0587,-746.1132;Inherit;False;Property;_MatteColor;MatteColor;9;0;Create;True;0;0;False;0;0,1,0,0;0,1,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;-417.8802,148.785;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-202.4014,733.3002;Inherit;False;Constant;_Float1;Float 1;12;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ComponentMaskNode;67;-267.3386,-454.9837;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector3Node;63;-381.1185,286.0243;Inherit;False;Constant;_Vector0;Vector 0;4;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ComponentMaskNode;61;-614.5183,751.915;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;64;-109.1621,318.0191;Inherit;False;Property;_Hide;Hide;10;0;Create;False;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;65;-339.4926,514.1837;Inherit;False;Property;_ChangeMatte;ChangeMatte;12;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;68;-129.4869,-115.5534;Inherit;False;Property;_ChangeMatte;ChangeMatte;12;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;145.0681,76.1013;Float;False;True;-1;2;ASEMaterialInspector;0;0;StandardSpecular;VGA/Hogawish/EyeShader;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;True;0;True;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;33;0;30;0
WireConnection;33;1;29;0
WireConnection;31;0;28;0
WireConnection;34;0;33;0
WireConnection;35;0;32;0
WireConnection;35;1;31;0
WireConnection;36;0;28;0
WireConnection;37;0;34;0
WireConnection;37;1;35;0
WireConnection;38;0;37;0
WireConnection;38;1;36;0
WireConnection;50;0;51;0
WireConnection;53;0;52;0
WireConnection;41;0;38;0
WireConnection;40;0;39;0
WireConnection;79;0;80;0
WireConnection;79;1;41;0
WireConnection;54;0;53;0
WireConnection;55;0;56;0
WireConnection;49;0;50;0
WireConnection;42;0;41;0
WireConnection;42;1;40;1
WireConnection;77;0;55;0
WireConnection;77;1;79;0
WireConnection;43;0;54;0
WireConnection;43;1;49;0
WireConnection;43;2;42;0
WireConnection;74;0;72;0
WireConnection;74;1;75;0
WireConnection;57;0;43;0
WireConnection;57;1;77;0
WireConnection;69;0;70;0
WireConnection;69;1;71;0
WireConnection;69;2;74;0
WireConnection;58;0;57;0
WireConnection;73;0;69;0
WireConnection;73;1;58;0
WireConnection;60;0;59;0
WireConnection;47;0;73;0
WireConnection;47;1;44;0
WireConnection;67;0;66;0
WireConnection;61;0;60;0
WireConnection;64;1;62;0
WireConnection;64;0;63;0
WireConnection;65;1;61;0
WireConnection;65;0;63;0
WireConnection;68;1;47;0
WireConnection;68;0;67;0
WireConnection;0;0;63;0
WireConnection;0;2;68;0
WireConnection;0;3;65;0
WireConnection;0;10;64;0
ASEEND*/
//CHKSM=551792B6400DFE40C263FD591A994D3035B7FA75