// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "VGA/Hogawish/WingShaderTransParent"
{
	Properties
	{
		_Txture1("Txture1", 2D) = "black" {}
		_Cutoff( "Mask Clip Value", Float ) = 0.04
		_ToonPower("ToonPower", Range( 0 , 1)) = 1
		_MatteColor("MatteColor", Color) = (0,1,0,0)
		[Toggle(_CHANGEMATTE_ON)] _ChangeMatte("ChangeMatte", Float) = 0
		_Texture1Grids("Texture1Grids", Vector) = (5,5,0,0)
		_Float1("Float 1", Float) = 0
		_Float3("Float 3", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Off
		CGINCLUDE
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _CHANGEMATTE_ON
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _Txture1;
		uniform float2 _Texture1Grids;
		uniform float _Float3;
		uniform float _ToonPower;
		uniform float4 _MatteColor;
		uniform float _Float1;
		uniform float _Cutoff = 0.04;

		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			// *** BEGIN Flipbook UV Animation vars ***
			// Total tiles of Flipbook Texture
			float fbtotaltiles94 = _Texture1Grids.x * _Texture1Grids.y;
			// Offsets for cols and rows of Flipbook Texture
			float fbcolsoffset94 = 1.0f / _Texture1Grids.x;
			float fbrowsoffset94 = 1.0f / _Texture1Grids.y;
			// Speed of animation
			float fbspeed94 = _Time[ 1 ] * _Float3;
			// UV Tiling (col and row offset)
			float2 fbtiling94 = float2(fbcolsoffset94, fbrowsoffset94);
			// UV Offset - calculate current tile linear index, and convert it to (X * coloffset, Y * rowoffset)
			// Calculate current tile linear index
			float fbcurrenttileindex94 = round( fmod( fbspeed94 + 0.0, fbtotaltiles94) );
			fbcurrenttileindex94 += ( fbcurrenttileindex94 < 0) ? fbtotaltiles94 : 0;
			// Obtain Offset X coordinate from current tile linear index
			float fblinearindextox94 = round ( fmod ( fbcurrenttileindex94, _Texture1Grids.x ) );
			// Multiply Offset X by coloffset
			float fboffsetx94 = fblinearindextox94 * fbcolsoffset94;
			// Obtain Offset Y coordinate from current tile linear index
			float fblinearindextoy94 = round( fmod( ( fbcurrenttileindex94 - fblinearindextox94 ) / _Texture1Grids.x, _Texture1Grids.y ) );
			// Reverse Y to get tiles from Top to Bottom
			fblinearindextoy94 = (int)(_Texture1Grids.y-1) - fblinearindextoy94;
			// Multiply Offset Y by rowoffset
			float fboffsety94 = fblinearindextoy94 * fbrowsoffset94;
			// UV Offset
			float2 fboffset94 = float2(fboffsetx94, fboffsety94);
			// Flipbook UV
			half2 fbuv94 = i.uv_texcoord * fbtiling94 + fboffset94;
			// *** END Flipbook UV Animation vars ***
			float4 tex2DNode7 = tex2D( _Txture1, fbuv94 );
			#ifdef _CHANGEMATTE_ON
				float4 staticSwitch36 = float4( (_MatteColor).rgb , 0.0 );
			#else
				float4 staticSwitch36 = ( tex2DNode7 * _ToonPower );
			#endif
			o.Emission = staticSwitch36.rgb;
			o.Alpha = _Float1;
			clip( tex2DNode7.r - _Cutoff );
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Unlit keepalpha fullforwardshadows exclude_path:deferred 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				SurfaceOutput o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutput, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17600
2022;562;1080;805;3028.551;332.0953;1;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;99;-2479.816,-242.3641;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;126;-2554.551,-39.09531;Inherit;False;Property;_Float3;Float 3;18;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;95;-2390.647,70.9334;Inherit;False;Property;_Texture1Grids;Texture1Grids;14;0;Create;True;0;0;False;0;5,5;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;94;-2100.312,-176.2043;Inherit;True;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;100;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TexturePropertyNode;4;-1856.734,-527.1248;Inherit;True;Property;_Txture1;Txture1;0;0;Create;True;0;0;False;0;None;630c6027569e3ba48b42db2f22c92839;False;black;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;7;-910.6622,-1103.215;Inherit;True;Property;_TextureSample0;Texture Sample 0;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-525.0728,-604.9795;Inherit;False;Property;_ToonPower;ToonPower;3;0;Create;True;0;0;False;0;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;31;-502.2597,-1187.832;Inherit;False;Property;_MatteColor;MatteColor;4;0;Create;True;0;0;False;0;0,1,0,0;0,1,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WireNode;119;-401.1984,-715.2979;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;178.8393,-479.4392;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;84;-515.8862,-63.15275;Inherit;False;1724.988;966.874;Outline;22;83;78;85;80;82;87;66;86;71;64;88;92;93;106;107;108;38;18;41;121;122;37;;1,1,1,1;0;0
Node;AmplifyShaderEditor.ComponentMaskNode;32;82.54137,-736.2791;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ComponentMaskNode;108;115.4539,226.9465;Inherit;False;True;True;True;False;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;86;531.5831,453.7119;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;122;710.9453,427.7345;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;41;258.4525,799.9955;Inherit;False;Constant;_Float0;Float 0;12;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;88;-353.2339,-1.940042;Inherit;True;Property;_OutlineColor;OutlineColor;10;0;Create;True;0;0;False;0;-1;None;630c6027569e3ba48b42db2f22c92839;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;66;297.2168,662.2065;Inherit;False;Property;_OutlineAmplify;OutlineAmplify;8;0;Create;True;0;0;False;0;0;0.04;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;78;-337.6841,324.0024;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;106;-271.9382,209.7025;Inherit;False;Property;_Color0;Color 0;5;0;Create;True;0;0;False;0;0,1,0,0;1,0.03301889,0.03301889,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;121;358.2285,219.2499;Inherit;True;Property;_OutlineAlpha;OutlineAlpha;17;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;102;-1654.483,31.85479;Inherit;True;Property;_Txture2;Txture2;2;0;Create;True;0;0;False;0;None;None;False;white;Auto;Texture2D;-1;0;1;SAMPLER2D;0
Node;AmplifyShaderEditor.Vector3Node;18;-202.561,740.3633;Inherit;False;Constant;_Vector0;Vector 0;4;0;Create;True;0;0;False;0;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;100;-920.5358,211.9992;Inherit;True;Property;_TextureSample4;Texture Sample 4;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;96;-2385.217,-74.32324;Float;False;Property;_speed;speed;12;0;Create;True;0;0;False;0;0;50;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;80;-360.4294,714.1437;Inherit;False;Property;_OutlineFrequencyX;OutlineFrequencyX;9;0;Create;True;0;0;False;0;0;18.07;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;87;-76.57869,536.4648;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;71;1031.258,427.7055;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;93;-340.5701,423.6398;Inherit;False;Property;_OutlineFrequencyT;OutlineFrequencyT;11;0;Create;True;0;0;False;0;0;15;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WireNode;125;-1246.359,133.4032;Inherit;False;1;0;SAMPLER2D;;False;1;SAMPLER2D;0
Node;AmplifyShaderEditor.StaticSwitch;37;141.2061,684.7268;Inherit;False;Property;_ChangeMatte;ChangeMatte;7;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;92;-75.47542,351.0916;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;118;803.969,-307.623;Inherit;False;Property;_Float1;Float 1;16;0;Create;True;0;0;False;0;0;1.67;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;101;-1624.657,520.7438;Inherit;False;Property;_Float2;Float 2;13;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;104;-1735.088,388.0004;Inherit;False;Property;_Texture2Grids;Texture2Grids;15;0;Create;True;0;0;False;0;5,5;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SinOpNode;82;386.085,435.7599;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;107;369.3793,49.95588;Inherit;False;Property;_ChangeMatte;ChangeMatte;7;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;36;694.3237,-788.5392;Inherit;False;Property;_ChangeMatte;ChangeMatte;9;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OutlineNode;64;901.2305,73.65216;Inherit;False;2;True;None;0;0;Front;3;0;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PosVertexDataNode;83;-505.807,515.7499;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;38;84.82383,313.824;Inherit;False;Property;_Hide;Hide;6;0;Create;True;0;0;False;0;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;85;92.46693,489.2524;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;103;-1722.256,264.7029;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCFlipBookUVAnimation;105;-1335.709,366.1605;Inherit;True;0;0;6;0;FLOAT2;0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;100;False;4;FLOAT;0;False;5;FLOAT;0;False;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1685.908,-689.9922;Float;False;True;-1;2;ASEMaterialInspector;0;0;Unlit;VGA/Hogawish/WingShaderTransParent;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.04;True;True;0;True;Transparent;;Geometry;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;94;0;99;0
WireConnection;94;1;95;1
WireConnection;94;2;95;2
WireConnection;94;3;126;0
WireConnection;7;0;4;0
WireConnection;7;1;94;0
WireConnection;119;0;7;0
WireConnection;20;0;119;0
WireConnection;20;1;19;0
WireConnection;32;0;31;0
WireConnection;108;0;106;0
WireConnection;86;0;82;0
WireConnection;86;1;66;0
WireConnection;122;0;121;1
WireConnection;122;1;86;0
WireConnection;100;0;125;0
WireConnection;100;1;105;0
WireConnection;87;0;83;1
WireConnection;87;1;80;0
WireConnection;71;1;122;0
WireConnection;125;0;102;0
WireConnection;37;0;18;0
WireConnection;92;0;78;0
WireConnection;92;1;93;0
WireConnection;82;0;85;0
WireConnection;107;1;88;0
WireConnection;107;0;108;0
WireConnection;36;1;20;0
WireConnection;36;0;32;0
WireConnection;64;0;107;0
WireConnection;64;1;71;0
WireConnection;38;1;41;0
WireConnection;38;0;18;0
WireConnection;85;0;92;0
WireConnection;85;1;87;0
WireConnection;105;0;103;0
WireConnection;105;1;104;1
WireConnection;105;2;104;2
WireConnection;0;2;36;0
WireConnection;0;9;118;0
WireConnection;0;10;7;1
ASEEND*/
//CHKSM=9257D79172F9E29967A3555530F41379A5197277