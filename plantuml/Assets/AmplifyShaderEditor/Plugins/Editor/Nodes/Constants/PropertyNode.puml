@startuml
enum PropertyType {
    Constant= 0,
    Property,
    InstancedProperty,
    Global,
}
enum VariableMode {
    Create= 0,
    Fetch,
}
class PropertyAttributes {
    + Name : string
    + Attribute : string
    + PropertyAttributes(name:string, attribute:string)
}
class PropertyNode {
    + PropertyNode()
    + PropertyNode(uniqueId:int, x:float, y:float, width:float, height:float)
    + <<override>> AfterCommonInit() : void
    + CheckDelayedDirtyProperty() : void
    + BeginPropertyFromInspectorCheck() : void
    + CheckPropertyFromInspector(forceUpdate:bool) : void
    + CheckLocalVariable(dataCollector:MasterNodeDataCollector) : bool
    + <<virtual>> ConfigureLocalVariable(dataCollector:MasterNodeDataCollector) : void
    + <<virtual>> CopyDefaultsToMaterial() : void
    + <<override>> SetupFromCastObject(obj:UnityEngine.Object) : void
    + ChangeParameterType(parameterType:PropertyType) : void
    InitializeAttribsArray() : void
    DrawAttributesAddRemoveButtons() : void
    CheckEnumAttribute() : void
    DrawEnumAddRemoveButtons() : void
    + <<virtual>> DrawAttributes() : void
    + <<virtual>> DrawMainPropertyBlock() : void
    + DrawMainPropertyBlockNoPrecision() : void
    + <<override>> DrawProperties() : void
    + ShowPrecision() : void
    + ShowToolbar() : void
    + ShowDefaults() : void
    + ShowPropertyInspectorNameGUI() : void
    + ShowPropertyNameGUI(isProperty:bool) : void
    + ShowVariableMode() : void
    + ShowAutoRegister() : void
    + <<virtual>> GetPropertyValStr() : string
    + <<override>> OnClick(currentMousePos2D:Vector2) : bool
    + <<override>> OnNodeDoubleClicked(currentMousePos2D:Vector2) : void
    + <<override>> DrawTitle(titlePos:Rect) : void
    + <<override>> Draw(drawInfo:DrawInfo) : void
    + <<override>> OnNodeLayout(drawInfo:DrawInfo) : void
    + <<override>> OnNodeRepaint(drawInfo:DrawInfo) : void
    + RegisterFirstAvailablePropertyName(releaseOldOne:bool) : void
    + SetRawPropertyName(name:string) : void
    + RegisterPropertyName(releaseOldOne:bool, newName:string, autoGlobal:bool, forceUnderscore:bool) : void
    + <<virtual>> CheckIfAutoRegister(dataCollector:MasterNodeDataCollector) : void
    + <<override>> GenerateShaderForOutput(outputId:int, dataCollector:MasterNodeDataCollector, ignoreLocalvar:bool) : string
    + <<override>> Destroy() : void
    BuildEnum() : string
    + PropertyAttributes : string <<get>>
    + <<virtual>> OnDirtyProperty() : void
    + <<virtual>> OnPropertyNameChanged() : void
    + <<virtual>> DrawSubProperties() : void
    + <<virtual>> DrawMaterialProperties() : void
    + <<virtual>> GetPropertyValue() : string
    + GetInstancedUniformValue(isTemplate:bool, isSRP:bool) : string
    + GetInstancedUniformValue(isTemplate:bool, isSRP:bool, dataType:WirePortDataType, value:string) : string
    + <<virtual>> GetUniformValue() : string
    + GetUniformValue(dataType:WirePortDataType, value:string) : string
    + <<virtual>> GetUniformData(dataType:string, dataName:string, fullValue:bool) : bool
    + <<override>> WriteToString(nodeInfo:string, connectionsInfo:string) : void
    IdForAttrib(name:string) : int
    + <<override>> ReadFromString(nodeParams:string[]) : void
    UpdateTooltip() : void
    + <<override>> SetClippedTitle(newText:string, maxSize:int, endString:string) : void
    + <<override>> SetClippedAdditionalTitle(newText:string, maxSize:int, endString:string) : void
    + <<override>> OnEnable() : void
    + CanDrawMaterial : bool <<get>>
    + RawOrderIndex : int <<get>>
    + OrderIndex : int <<get>> <<set>>
    + OrderIndexOffset : int <<get>> <<set>>
    + PropertyData(portCategory:MasterNodePortCategory) : string
    + <<override>> OnNodeLogicUpdate(drawInfo:DrawInfo) : void
    + ShowGlobalValueButton() : void
    + PropertyNameFromTemplate(data:TemplateShaderPropertyData) : void
    + <<virtual>> GeneratePPSInfo(propertyDeclaration:string, propertySet:string) : void
    + <<virtual>> SetGlobalValue() : void
    + <<virtual>> FetchGlobalValue() : void
    + <<virtual>> PropertyName : string <<get>>
    + <<virtual>> PropertyInspectorName : string <<get>>
    + FreeType : bool <<get>> <<set>>
    + ReRegisterName : bool <<get>> <<set>>
    + CustomPrefix : string <<get>> <<set>>
    + <<override>> RefreshOnUndo() : void
    + <<override>> DataToArray : string <<get>>
    + RegisterPropertyOnInstancing : bool <<get>> <<set>>
    + SrpBatcherCompatible : bool <<get>>
    + AddGlobalToSRPBatcher : bool <<get>> <<set>>
}
ParentNode <|-- PropertyNode
PropertyNode --> "CurrentParameterType" PropertyType
PropertyNode --> "CurrentVariableMode" VariableMode
@enduml
